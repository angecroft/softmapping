#ifndef MOLD_H
#define MOLD_H

#include "ofPoint.h"
#include "Reshapable.h"

/********************************************//**
 *  \file Mold.h
 *  \brief Mold (Mapping Unit)
 *
 *  Selection Mold which will be send to the second step
 ***********************************************/

class Mold: public Reshapable
{
    public:
        Mold(ofPoint R);
        virtual ~Mold();
        void setDimensions(float w, float h); // Set height and width

};

#endif // MOLD_H
