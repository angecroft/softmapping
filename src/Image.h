#ifndef IMAGE_H
#define IMAGE_H

#include "ofImage.h"
#include "Source.h"
#include <string>

/********************************************//**
 *  \file Image.h
 *  \brief Image Source
 *
 *
 ***********************************************/

class Image : public Source
{
    public:
        //Image();
        Image(string pth, char typ){
            data.loadImage(pth);
            type= typ;

            //cout << " Img loaded" << endl;
            cerr << "Img! > ";
        }
        virtual ~Image();

        void bind(); //?
        void clear(); //?
        virtual void draw(ofPoint p);
        //void draw(int x, int y);

        void saveImage(string fileName);
        void loadImage(string fileName);

        void crop(int x, int y, int w, int h);
        //void grabScreen(int x, int y, int w, int h);
        void mirror(bool vertical, bool horizontal);
        void resize(int newWidth, int newHeight);

        ofImage getData();
        virtual float getWidth();
        virtual float getHeight();

    //protected:
    private:
        ofImage data;
};

#endif // IMAGE_H
