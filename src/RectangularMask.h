#ifndef RECTANGULARMASK_H
#define RECTANGULARMASK_H

#include "Mask.h"

/*****************************************/
/* CLASSE CONDAMNEE ( cf RectMask )     */
/****************************************/


class RectangularMask : public Mask_
{
    public:
        RectangularMask();
        virtual ~RectangularMask();

        virtual int getWidth();
        virtual int getHeight();

        void setWidth(int other_width);
        void setHeight(int other_height);

        virtual void drawFrame();
        virtual void drawContent_();

        //virtual bool inside(int x, int y);

    protected:
   // private:
        bool getIsselected();
        void setIsselectedOff();
        void setIsselectedOn();

        int width;
        int height;
        bool isSelected;
};

#endif // RECTANGULARMASK_H
