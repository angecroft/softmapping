#include "RectMold.h"
#include "ofGraphics.h"

RectMold::RectMold(ofPoint R): Mold(R)
{
    //ctor
    width= 0;
    height= 0;
    cerr << "Rect" << endl;
}

RectMold::~RectMold()
{
    //dtor
}

void RectMold::drawContent_()
{
    ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 0, 255, 100);
        ofRect(origin, width, height);
        ofDisableAlphaBlending();
        ofSetColor(255, 255, 255);
    ofPopStyle();
}
