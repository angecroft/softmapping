#pragma once

#include "ofMain.h"
#include "Source.h"
#include "Image.h"
#include "Video.h"
#include "ofxGLWarper.h"
#include "Reshapable.h"

/********************************************//**
 *  \file Layer.h
 *  \brief Layer
 *
 *
 ***********************************************/

class Layer: public Reshapable {

	public:

	    Layer(char type, string path, float x, float y, ofPoint R);

	    void drawContent_();

	    int getAlpha();
        void setAlpha( int pInt );
        void addAlpha(int i);
        void subAlpha(int i);

        bool getLock();
        void toggleLock();

        char getType();

        void onCornerChange(ofxGLWarper::CornerLocation & cornerLocation);

	    Source* src;    /**< Source associated with */

	    bool visible;
	    bool locked;


	    protected:
            int alpha;  /**< Value of opacity */

};
