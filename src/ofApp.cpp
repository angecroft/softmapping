#include "ofApp.h"
#include "MapIn.h"
#include "MapOut.h"

// EVENT DISPATCHER
bool news= false;
//--------------------------------------------------------------
void ofApp::setup(){

    a_states.push_back(new MapIn());
    a_states.push_back( new MapOut(originCompOut, compWidth, compHeight) );

    currState= 0;
    mapout= false;
    a_states[0]->setup();

    ofSetFrameRate(60);
	ofSetWindowPosition(50, 50);
	ofSetWindowTitle("MAIN WINDOW");

	open= false;


}

//--------------------------------------------------------------
void ofApp::update(){

    a_states[currState]->update();
    MapOut *mo = (MapOut*)a_states[1];
    news= mo->hasNews();
}

//--------------------------------------------------------------
void ofApp::draw(){

    a_states[currState]->draw();
    //ofDrawBitmapString( "Appuyer sur 'f' pour passer en fullscreen", 60, 60);
    //ofDrawBitmapString( "Press 'C' >> Map", 60, 45);
    if( mapout )
        ofDrawBitmapString( "Please create a layer and a mold first !", 600, 80);

    if( open && news ){


        MapOut *mo = (MapOut*)a_states[1];
        ofImage* tmp= mo->renderOut();
        mo->news= false;
        //window.content->allocate(tmp->getWidth(), tmp->getHeight(), OF_IMAGE_COLOR);
        window.content= tmp;
        window.vect.set(mo->vect);
        window.OW.set(mo->OW);
        window.srcWidth= mo->getWidth_();
        window.srcHeight= mo->getHeight_();
        //window.EW.set(mo->EW);
    }

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

    if( key == 'f')
        ofToggleFullscreen();

    if( key == 'c')
    {

        MapIn *mi = (MapIn*)a_states[0];

        if( mi->nb_Lay == 0 || mi->select == NULL )
            mapout = true;
        else{
            mapout= false;
            MapOut *mo = (MapOut*)a_states[1];

            for(int i = 0; i<mi->gui.size(); ++i)
            {
                mi->gui[i]->toggleVisible();
            }

            mi->MoldMenu->toggleVisible();

            if(currState == 0 && mi->select != NULL)
                    mo->dataIn = mi->moldOut();


            currState = !currState;
            a_states[1]->setup();
        }
    }

    if( key == 'o' && currState && !open ){
        MapOut *mo = (MapOut*)a_states[1];
        //window.content=  mo->renderOut();
        //window.vect.set(mo->vect);
        ofxFensterManager::get()->setupWindow(&window);
        window.windowResized(1024,768);
        open= true;
    }
    else
        a_states[currState]->keyPressed(key);

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

    a_states[currState]->keyReleased(key);

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

    a_states[currState]->mouseMoved(x, y);

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

    a_states[currState]->mouseDragged(x, y, button);
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    //clic gauche 0
    //clic droit  2
    cout << "MOUSE ! (" << x << ", " << y << ")" << endl;
    a_states[currState]->mousePressed(x, y, button);

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    a_states[currState]->mouseReleased(x, y, button);

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
