#ifndef UI_H
#define UI_H

#include "ofxUI.h"
#include "Layer.h"

class LightUI {

    LightUI(){
        selectedGui= 0;


    }

    void init(){

        globalMenu = new ofxUICanvas();

        globalMenu->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
        globalMenu->addMultiImageToggle("mouse", "GUI/toggle.png", true);
        globalMenu->addMultiImageToggle("warp", "GUI/toggle.png", false);
        globalMenu->addMultiImageToggle("lockAll", "GUI/toggle.png", false);
        globalMenu->addSpacer(1, 18);

        globalMenu->addMultiImageToggle("browseVideo", "GUI/toggle.png", false);
        globalMenu->addMultiImageToggle("browseImg", "GUI/toggle.png", false);

        globalMenu->addSpacer(1, 18);
        globalMenu->addMultiImageToggle("grid", "GUI/toggle.png", false);

        globalMenu->autoSizeToFitWidgets();
        globalMenu->setPosition(0, 0 );

        //ofAddListener(globalMenu->newGUIEvent, this, &MapIn::guiEvent);

    }

    vector<ofxUISuperCanvas *> elmts;
    ofxUICanvas *globalMenu;
    int selectedGui;
    ofxUICanvas *MoldMenu;
    bool updateMold;

};
//{
//    public:
//        UI(Layer *layers_, int *selectedLayer_);
//        ~UI();
//
//        vector<ofxUISuperCanvas *>gui;
//		ofxUICanvas *globalMenu;
//		int selectedUILayer; /** selectedUILayer : la GUI (concernant les layers) selectionnée  */
//		Layer *layers;
//		int *selectedLayer;
//
//        /* General */
//		void setColorGui(ofxUISuperCanvas& gui,int type);
//        void guiEvent(ofxUIEventArgs &e);
//        void exit();
//
//        /* Layers */
//        void addLayerUI(Layer &layer, int selectedLay);
//        void setLayerUI(Layer &layer, int numLay);
//        void updateLayerUI(int selectedLayer, Layer *a_layers);
//        void deleteLayerUI(int layerToDel);
//
//        /* Masks */
//
//        /* Mold */
//
//        /* Global Menu */
//        void setGlobalMenu();
//};
//
//#endif // UI_H
