#ifndef RECTNORMALMASK_H
#define RECTNORMALMASK_H

/*****************************************/
/* CLASSE CONDAMNEE ( cf RectMask )     */
/****************************************/

#include "RectMask.h"

class RectNormalMask : public RectMask
{
    public:
        RectNormalMask(ofPoint R);
        ~RectNormalMask();
        void drawPreview();
        void drawContent_();
        void updateImg(ofPoint p){
            img.grabScreen(p.x, p.y, width, height );
        }
    protected:
    private:
};

#endif // RECTNORMALMASK_H
