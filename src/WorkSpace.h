#pragma once

#include "ofMain.h"

/********************************************//**
 *  \file WorkSpace.h
 *  \brief Layout
 *
 *
 ***********************************************/

// INTERFACE

class WorkSpace {

	public:

        void drawViewportOutline(const ofRectangle & viewport){ //virtual possible
            ofPushStyle();
            ofFill();
            ofSetColor(50);
            ofSetLineWidth(0);
            ofRect(viewport);
            ofNoFill();
            ofSetColor(25);
            ofSetLineWidth(1.0f);
            ofRect(viewport);
            ofPopStyle();
        }

        //virtual void toggleGrid(){ }

        virtual void draw_start(){

            ofRectangle view( origin.x, origin.y, width, height );

            drawViewportOutline(view);
            ofPushView();
            ofViewport(view);
            ofSetupScreen();
        }

        void draw_end(){ //virtual possible
            ofPopView();
        }

        virtual void leftClic(int x, int y )= 0;
        virtual void dragLeft(int x, int y )= 0;
        //virtual void rightClic(int x, int y )= 0;
        //virtual void draw()= 0;

        ~WorkSpace() { }

        void move_ws( int x, int y){
            origin.set(x, y);
        }

        void scale_ws( int x, int y){
            width= x - origin.x;
            height= y - origin.y;

            if (width < 20)
                width= 20;

            if (height < 20)
                height= 20;
        }

          ofPoint origin;
          float width;
          float height;

};
