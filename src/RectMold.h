#ifndef RECTMOLD_H
#define RECTMOLD_H

#include "Mold.h"
#include "ofRectangle.h"

/********************************************//**
 *  \file RectMold.h
 *  \brief Rectangle Mold
 *
 *  Inherited from Mold. Rectangle-shaped mold
 ***********************************************/


class RectMold : public Mold
{
    public:
        RectMold(ofPoint R);
        virtual ~RectMold();
        void drawContent_();


};

#endif // RECTMOLD_H
