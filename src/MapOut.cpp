#include "MapOut.h"
#include "CompWspace.h"
#include "RectMask.h"
#include "RectOpacityMask.h"
#include "RectColorMask.h"
#include "RectNormalMask.h"
#include "RectAlphaBlendMask.h"

bool bloc = false;
//--------------------------------------------------------------
void MapOut::setup(){
    nb_Masks= 0;
    selectMask = -1;

    nb_WS= 1;
    a_wspaces= new WorkSpace*[nb_WS];

    int Wid= 700;
    int Hei= 500;
    vect.set(0,0);

    a_wspaces[0]= new CompWspace(originCompOut.x, originCompOut.y, Wid, Hei);

    a_wspaces[1]= new CompWspace(originCompOut.x + Wid, originCompOut.y, 200, 200);

    selected = false;
    edges= false;
    news= false;

}

//--------------------------------------------------------------
void MapOut::update(){
            /*
        if( vid != NULL )
            vid->update();
                */

    OW.set( (getWarpOrigin()- ref) - getOrigin_() );

   // ofPoint Oend= getOrigin_() + ofPoint(getWidth_(), getHeight_());
    //EW.set( (getWarpEnd()- ref) - Oend );
}

void MapOut::addMask( char type ){


    switch (type){

        case 'a':
            a_Masks.push_back( new RectAlphaBlendMask(originCompOut) );
            break;

        case 'c':
            a_Masks.push_back( new RectColorMask(originCompOut) );
            break;

        case 'o':
            a_Masks.push_back( new RectOpacityMask(originCompOut) );
            break;

        default:
            a_Masks.push_back( new RectNormalMask(originCompOut) );
            cerr << "Unknown type of Mask" << endl;
    }
    selectMask= nb_Masks;
    ++nb_Masks;
    news= true;
}

void MapOut::clearMasks(){

    nb_Masks= 0;
    a_Masks.clear(); /* hum... */
    news= true;
}

void MapOut::updateAllMasks(){
    if (!bloc)
    for(int i= 0; i < nb_Masks; ++i)
        a_Masks[i]->updateImg( a_Masks[i]->getOrigin_() + ofPoint( originCompOut.x, -originCompOut.y-41) );
                                                                                    /* ^^^^???^^^^^^^ */

}

void MapOut::drawMasksPreview(){
    for(int i= 0; i < nb_Masks; ++i)
        a_Masks[i]->drawPreview();

}

void MapOut::drawMasksContent(){
    for(int i= 0; i < nb_Masks; ++i)
        a_Masks[i]->drawContent_();

}

void MapOut::drawMasksFrames(){

    for(int i= 0; i < nb_Masks; ++i)
        a_Masks[i]->drawFrame();
}

ofImage MapOut::grabMask( int i ){
    ofImage temp;
    temp.grabScreen(a_Masks[i]->getOrigin_().x, a_Masks[i]->getOrigin_().y, a_Masks[i]->getWidth_(), a_Masks[i]->getHeight_());
    return temp;
}

ofImage* MapOut::renderOut(){

    ofImage* out= new ofImage();

    out->grabScreen(originCompOut.x + 700, originCompOut.y, 200, 200);

    return out;
}

MapOut::MapOut( ofPoint R, float w, float h): Reshapable(R, w, h){

 }

bool MapOut::hasNews( ){

    return news;
}

void MapOut::drawContent_(){

    ofPushMatrix();
    ofTranslate( -ref );
            warper.begin();

                ofPushStyle();

                //ofEnableAlphaBlending();
                ofSetColor(255,255,255 );
                dataIn.resize(200,200);
                dataIn.draw(0,0);//dataIn.draw(origin.x,origin.y); //dataIn.draw(412,240);

                ofPopStyle();

            warper.end();
    ofPopMatrix();

    //vid->draw( origin + OV, vid->getWidth(), vid->getHeight() );


}

//--------------------------------------------------------------
void MapOut::draw(){

    ofPushStyle();
    //ofEnableAlphaBlending();
    ofBackgroundGradient(ofColor(255, 255, 255), ofColor(0, 0, 0), OF_GRADIENT_CIRCULAR);

    ofDrawBitmapString( "MapOut", 940, 30);

    /*a_wspaces[1]->draw_start();

        this->drawContent_();
        drawMasksContent();

    a_wspaces[1]->draw_end();*/

    ofImage preview;

    a_wspaces[0]->draw_start();

        ofPushStyle();
            ofSetColor(0);
            ofRect(ofPoint(), 700, 500);
        ofPopStyle();

    this->drawContent_();

    if( edges )
        drawEdges();

    //preview.grabScreen( getOrigin_().x + ref.x/* - originCompOut.x*/, getOrigin_().y + ref.y /*- originCompOut.y*/, 1000, 1000 );
    a_wspaces[0]->draw_end();

    preview.grabScreen( getWarpOrigin().x, getWarpOrigin().y, this->warpWidth(), this->warpHeight() );
    //preview.grabScreen( getOrigin_().x + ref.x, getOrigin_().y + ref.y, dataIn.width, dataIn.height );

    a_wspaces[0]->draw_start();

        ofPushStyle();
            ofSetColor(0);
            ofRect(ofPoint(), 700, 500);
        ofPopStyle();


        this->drawContent_();
        updateAllMasks();

        if( selected )
            this->drawSelection_();

        drawMasksFrames();
        //drawMasksContent();

    a_wspaces[0]->draw_end();

    a_wspaces[1]->draw_start();

        preview.resize( 200, 200 );
        preview.draw(0,0);

        ofPushStyle();
            ofSetColor( 0 );
            ofFill();
            for(int i=0; i < nb_Masks; ++i)
                ofRect(a_Masks[i]->OM.x, a_Masks[i]->OM.y, a_Masks[i]->getWidth_(), a_Masks[i]->getHeight_());
        ofPopStyle();

        drawMasksPreview();

    a_wspaces[1]->draw_end();

    if( this->isInWarpMode() )
            ofDrawBitmapString( "WARP MODE", 600, 25+15);

    if( infoKeys ){
        ofDrawBitmapString( "w:  Warp mode", 820, 320);
        ofDrawBitmapString( "e:  Show edges", 820, 340);
        ofDrawBitmapString( "k:  Alpha blend Mask", 820, 360);
        ofDrawBitmapString( "l:  Color Mask", 820, 380);
        ofDrawBitmapString( "m:  Opacity Mask", 820, 400);
        ofDrawBitmapString( "y:  Increase effect", 820, 420);
        ofDrawBitmapString( "y:  Decrease effect", 820, 440);
        ofDrawBitmapString( "r:  Reset Mask", 820, 460);
        ofDrawBitmapString( "n:  Delete masks", 820, 480);
    }

    ofPushStyle();
        ofSetColor(255, 0, 55);
            ofLine( ofPoint(10,40), ofPoint(10,40) + EW );
    ofPopStyle();

    ofPopStyle();


}

void MapOut::drawEdges(){ // REF - WIDTH - HEIGHT - WARP - ANCHOR

    ofPushMatrix();
    ofTranslate( -ref );//ofTranslate( -260, -100 );
    warper.begin();
        ofPushStyle();

            ofSetColor(230,245,0);
            ofNoFill();
            ofSetLineWidth(1.5);
            //ofRect( 0, 0, p.x - origin.x, p.y - origin.y);
            ofRect( 0, 0, contWidth, contHeight );

        ofPopStyle();
    warper.end();
    ofPopMatrix();
}

//--------------------------------------------------------------
void MapOut::keyPressed(int key){
    if( key == 'k')
            addMask('a');
    if( key == 'l')
            addMask('c');
    if( key == 'm')
            addMask('o');
        /*
    if( key == 'b' )
        bloc = !bloc;
        */

    if( key == 'i')
        infoKeys= !infoKeys;
            //addMask('?');

    if( key == 'n')
            clearMasks();

    if( key == 'g' )
        toggleGrid();


    if( key == 'w')
        if( this->isInWarpMode() )
            this->disableWarper();
        else

            this->enableWarper();

        /*
    if( key == 112) // p
         vid->play();
         */

    if( key == 'e' )
        edges= !edges;

    if( selectMask != -1 ){
        if( key == 'y' )
            a_Masks[selectMask]->changeParam("up");
        if( key == 't' )
            a_Masks[selectMask]->changeParam("down");
        if( key == 'r' )
            a_Masks[selectMask]->changeParam("reset");
    }

    news= true;

}

//--------------------------------------------------------------
void MapOut::keyReleased(int key){

}

//--------------------------------------------------------------
void MapOut::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void MapOut::mouseDragged(int x, int y, int button){

    if( 'r' == drag )
        a_Masks[selectMask]->resize__(x, y);

    if( 'm' == drag ){
        if( 0 != nb_Masks ){

            a_Masks[selectMask]->move_( x-(pressed.x - a_Masks[selectMask]->getOrigin_().x ), y-(pressed.y - a_Masks[selectMask]->getOrigin_().y ) );

            pressed.x = x;
            pressed.y = y;

            a_Masks[selectMask]->updateOM( getOrigin_() );
        }
    }

    if( 'Z' == drag )
        this->resize__(x, y);

    if( 'M' == drag ){

        this->move_(x-(pressed.x - origin.x ), y-(pressed.y - origin.y ));

        pressed.x = x;
        pressed.y = y;

        for(int i= 0; i < nb_Masks; ++i)
            a_Masks[i]->updateOM( getOrigin_() );

        vect.set( getOrigin_() );
    }

    news= true;
}

int MapOut::currMask( float x, float y ){

    ofPoint relPos ( ofPoint(x,y) - a_wspaces[0]->origin );

    if( 0 != nb_Masks )
        for(int k = nb_Masks-1; k >= 0; --k){
            if( a_Masks[k]->inside_(ofPoint(x,y)) )
                return k;
        }

    return -1;
}

//--------------------------------------------------------------
void MapOut::mousePressed(int x, int y, int button){

    //if in good ws
    int currM= currMask(x, y);
    cout << currM <<endl;

    pressed.set(x, y);
    cout << pressed <<endl;

    switch( button ){

        case 0: //Left Clic
            if( this->isInWarpMode() )
                    break;

            selectMask= currM;
            if( selectMask != -1 ){
                if( a_Masks[selectMask]->insideScaleArea_(x, y) ){
                    drag = 'r';
                    break;
                }
                drag= 'm'; //move mask
            }
            else{
                //if( ofRectangle( origin + a_wspaces[0]->origin, dataIn.width, dataIn.height ).inside( x, y ) ){
                if( this->insideScaleArea_(pressed.x, pressed.y) ){
                    drag = 'Z';
                    break;
                }
                if( this->inside_(pressed) ){
                    drag= 'M'; //move mold
                    selected = true;
                }
                else
                    selected = false;
            }
            break;
    }
}

//--------------------------------------------------------------
void MapOut::mouseReleased(int x, int y, int button){
    drag= '?';
}

//--------------------------------------------------------------
void MapOut::windowResized(int w, int h){

}

//--------------------------------------------------------------
void MapOut::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void MapOut::dragEvent(ofDragInfo dragInfo){

}
