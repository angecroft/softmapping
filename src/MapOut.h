#pragma once

#include "ofMain.h"
#include "AppState.h"
#include "Image.h"
#include "Mask.h"
#include "ofxGLWarper.h"
#include "Reshapable.h"

/********************************************//**
 *  \file MapOut.h
 *  \brief Mapping OUT
 *
 *  Second step of the application. Render Editor
 ***********************************************/

const ofPoint originCompOut( 100, 100 );
const float compWidth= 200;
const float compHeight= 200;

class MapOut : public AppState, public Reshapable{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

        MapOut( ofPoint R, float w, float h);
		void drawContent_();

		virtual int currWorkspace( float x, float y) { return 0; };
		int currMask( float x, float y );

		void addMask( char type );
		void clearMasks();
		//void drawMasks();
		void updateAllMasks();
		void drawMasksPreview();
		void drawMasksContent();
        void drawMasksFrames();
        void drawEdges();
        bool hasNews( );
       // void drawSelection(bool warp); // *
        ofImage grabMask( int i );
		ofImage* renderOut();

        bool selected;
        ofImage dataIn;
        char drag;

        vector<Mask_*> a_Masks;
        int nb_Masks;
        int selectMask;

        bool edges;
        bool news;

        ofVec2f OW;
        ofVec2f EW;
        ofVec2f vect;
};
