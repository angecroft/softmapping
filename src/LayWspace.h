#pragma once

#include "ofMain.h"
#include "WorkSpace.h"

/********************************************//**
 *  \file LayWspace.h
 *  \brief Layers Layout
 *
 *  Layout where buttons and layer thumbnails are available
 ***********************************************/

class LayWspace: public WorkSpace {

	public:

        virtual void leftClic(int x, int y );
        void rightClic(int x, int y );
        virtual void dragLeft(int x, int y );
        virtual void draw();

        LayWspace(){

            origin.set( 20, 90 );
            width= 90;
            height= 90;
        }

        LayWspace( float x, float y, float w, float h)
            /*: x_origin(x), y_origin(y), width(w), height(h)NE MARCHE PAS ?*/{
            cout << "LaySpace created" << endl;
            origin.set( x, y );
            width= w;
            height= h;
            action = 'n';
            cerr << "Lay" << endl;
        }

        ~LayWspace() { }

        bool insideMoveDiv(int x, int y);
        bool insideScaleDiv(int x, int y);
        char action;

};
