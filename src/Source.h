#ifndef SOURCE_H
#define SOURCE_H

#include "ofMain.h"
#include <string>

class Source
{
        public:
        virtual ~Source() { }

        virtual void draw(ofPoint p) = 0;
        virtual float getWidth() = 0;
        virtual float getHeight() = 0;
        char getType(){ return type; }


    protected:
        std::string path;
        char type;
        //int alpha;

};

#endif // SOURCE_H
