#pragma once

#include "ofMain.h"
#include "AppState.h"
#include "Layer.h"
#include "Mold.h"

#include "ofxUI.h"

/********************************************//**
 *  \file MapIn.h
 *  \brief Mapping IN
 *
 *  First step of the application. Sources composer
 ***********************************************/

class MapIn : public AppState{

	public:
	    //ofBase
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		void updateOV();

        //MapIn
		virtual int currWorkspace( float x, float y );
        int currLayer( float x, float y, int ws );

        //Layers
		void addNewLay(char type, string path);
		void supprLay();
		void backward(int layNum);
		void forward(int layNum);
		void toBack(int layNum);
		void toFront(int layNum);

		//GUI

		// General functions
		void exit();
		void setColorGui(ofxUISuperCanvas& gui,int type);
        void guiEvent(ofxUIEventArgs &e);

        // Gui Layers
        void setGUILayer(Layer &layer, int numLay);
        void addGUILayer(Layer &layer, int selectLay);
        void updateGUILayer();
        void updatePosGUILayer();
        void updateColorSelectedGUI();
        void deleteGUILayer();

        // Gui Molds
        void setGUIMolds();
        void upgradeGUIMold();
        void updateGUIMold();

        // Global Menu
        void setGUIMenu();
        void updateGlobalMenu();


		void magnet(float xO, float yO);

        //Molds
		ofImage moldOut();
		Video* getVideo();
		//vector<ofImage> moldOut2();

		vector<Layer*> a_layers;
		vector<int> layRank;
		int nb_Lay;
		int selectLay;
		int layerDeleted;

		Mold *select;
		ofImage tempOut; //useless; to delete
		char drag; //move in AppState

		//gui
		vector<ofxUISuperCanvas *>gui; //layers
		ofxUICanvas *globalMenu;
		int selectedGui;
		ofxUICanvas *MoldMenu;
		bool updateMold;

};
