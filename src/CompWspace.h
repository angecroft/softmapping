#pragma once

#include "ofMain.h"
#include "WorkSpace.h"

/********************************************//**
 *  \file CompWspace.h
 *  \brief Compositing Layout
 *
 *
 ***********************************************/

class CompWspace: public WorkSpace {

	public:

        virtual void leftClic(int x, int y );
        void rightClic(int x, int y );
        virtual void dragLeft(int x, int y );
        void drawGrid(float dx, float dy);
        void draw();

        virtual void draw_start(){

            WorkSpace::draw_start();
            //if( grid )


        }

        CompWspace(){

            origin.set(20, 90);
            width= 90;
            height= 90;

            cerr << "Comp" << endl;

        }

        CompWspace( float x, float y, float w, float h)
            /*: x_origin(x), y_origin(y), width(w), height(h)NE MARCHE PAS ?*/{
            cout << "LaySpace created" << endl;
            origin.set( x, y );
            width= w;
            height= h;
            action = 'n';
            viewport2D.x = x;
            viewport2D.y = y;
            viewport2D.width = w;
            viewport2D.height = h;
        }

        ~CompWspace() { }

        void drawViewportOutline(const ofRectangle & viewport);

        bool insideMoveDiv(int x, int y);
        bool insideScaleDiv(int x, int y);
        char action;


        ofRectangle viewport2D;

};
