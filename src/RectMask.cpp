#include "RectMask.h"
#include "ofGraphics.h"

RectMask::RectMask(ofPoint R): Mask_(R),
    isSelected(false)
{
    cerr << "Rect" << endl;
    width = 0;
    height = 0;
    //ctor
}

RectMask::~RectMask()
{
    //dtor
}

void RectMask::changeParam(string s){

}

void RectMask::drawFrame() // les -2 et +2 sont laissé car ils oivent avoir un interet
{

    ofSetColor(255,0,0);
    ofNoFill();
    ofRect(getOrigin_(), width, height); //Relative

    ofSetColor(255);

}

void RectMask::drawPreview(){
    ofPushStyle();
        ofSetColor(255,255,255);
        img.draw(OM);
    ofPopStyle();
}

void RectMask::drawContent_(){
    ofPushStyle();
        ofSetColor(255,255,255);
        img.draw(origin);
    ofPopStyle();
}


bool RectMask::getIsselected()
{
    return isSelected;
}

void RectMask::setWidth(int other_width)
{
    this->width=other_width;
}

void RectMask::setHeight(int other_height)
{
    this->height=other_height;
}

void RectMask::setIsselectedOff()
{
    this->isSelected=false;
}

void RectMask::setIsselectedOn()
{
    this->isSelected=true;
}

bool RectMask::inside(int x, int y)
{
    return ofRectangle( origin, width, height ).inside(x,y);

    /*if(x > getOrigin().x && y > getOrigin().y && x < (getOrigin().x + this->width) && y < (getOrigin().y + this->height))
        return true;
    else
        return false;*/
}




