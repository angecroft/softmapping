#include "Image.h"
#include "ofMain.h"

/*Image::Image(){
}*/

//***************************************************************

/*Image::Image(string pth, char typ){

}*/

//***************************************************************

Image::~Image(){
    //dtor
}

//***************************************************************

void Image::bind(){ //utilit� ?
    data.bind();
    return;
}

//***************************************************************

void Image::clear(){ //utilit� ?
    data.clear();
    return;
}

//***************************************************************

void Image::draw(ofPoint p){

    ofEnableAlphaBlending();
    //data.draw(- getWidth()/2,- getHeight()/2);
    data.draw(p.x,p.y);

    ofDisableAlphaBlending();
    return;
}

    /*
void Image::draw(int x, int y){
    data.draw(x,y);
}
    */

//***************************************************************

/*void Image::update() {
    Source::update();
}*/

//***************************************************************

void Image::saveImage(string fileName){
    data.saveImage(fileName);
    return;
}

//***************************************************************

void Image::loadImage(string fileName){

    data.loadImage(fileName);
    return;
}


//***************************************************************

void Image::crop(int x, int y, int w, int h){
    data.crop(x,y,w,h);
    return;
}

    /*
void Image::grabScreen(int x, int y, int w, int h){
    data.grabScreen(x,y,w,h);
}
    */

//***************************************************************

void Image::mirror(bool vertical, bool horizontal){
    data.mirror(vertical, horizontal);
    return;
}

//***************************************************************

void Image::resize(int newWidth, int newHeight){
    data.resize(newWidth, newHeight);
    return;
}

//***************************************************************

ofImage Image::getData(){
    return data;
}

//***************************************************************

float Image::getWidth(){
    return data.getWidth();
}

//***************************************************************

float Image::getHeight(){
    return data.getHeight();
}

