#ifndef RECTOPACITYMASK_H
#define RECTOPACITYMASK_H

#include "RectMask.h"

class RectOpacityMask : public RectMask
{
    public:
        RectOpacityMask(ofPoint R);
        virtual ~RectOpacityMask();
        void drawPreview();
        void drawContent_();
        void changeParam(string s);
        void updateImg(ofPoint p){
            img.grabScreen(p.x, p.y, width, height );
        }
    protected:
        int opacity;
    private:
};

#endif // RECTOPACITYMASK_H
