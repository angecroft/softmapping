//#include "UI.h"
//#include "File.h"
//#include "MapIn.h"
//
//    UI::UI(Layer *layers_, int *selectedLayer_){
//        selectedLayer = selectedLayer_;
//        layers = layers_;
//    }
//
//    UI::~UI(){
//        exit();
//    }
//
//    /* ************************************ */
//    /* ************* General ************** */
//    /* ************************************ */
//
//    void UI::setColorGui(ofxUISuperCanvas& gui,int type = 0){
//        if(type == 0){
//        ofxUIColor cb = ofxUIColor( 70, 70, 70, 180 ); // background
//        ofxUIColor co = ofxUIColor( 0, 0, 0, 255 ); // ?
//        ofxUIColor coh = ofxUIColor( 0, 0, 0, 255 ); // textArea:hover
//        ofxUIColor cf = ofxUIColor( 185, 222, 81, 255 ); // lime color
//        ofxUIColor cfh = ofxUIColor( 209, 227, 137, 255 ); // textArea:inside
//        ofxUIColor cp = ofxUIColor( 0, 0, 0, 255 ); // ?
//        ofxUIColor cpo = ofxUIColor( 0, 0, 0, 255 ); // ?
//            gui.setUIColors( cb, co, coh, cf, cfh, cp, cpo );
//        }
//        else{
//            ofxUIColor cb = ofxUIColor( 70, 70, 70, 180 ); // background
//            ofxUIColor co = ofxUIColor( 0, 0, 0, 255 ); // ?
//            ofxUIColor coh = ofxUIColor( 0, 0, 0, 255 ); // textArea:hover
//            ofxUIColor cf = ofxUIColor( 41, 193, 51, 255 ); // lime color
//            ofxUIColor cfh = ofxUIColor( 209, 227, 137, 255 ); // textArea:inside
//            ofxUIColor cp = ofxUIColor( 0, 0, 0, 255 ); // ?
//            ofxUIColor cpo = ofxUIColor( 0, 0, 0, 255 ); // ?
//            gui.setUIColors( cb, co, coh, cf, cfh, cp, cpo );
//        }
//    }
//
//    void UI::guiEvent(ofxUIEventArgs &e){
//        string name = e.getName();
//
//        // -------- LAYERS -----------------------------------------------------
//        if(name == "posX" && selectedUILayer >= 0){
//            if( !layers[selectedUILayer].getLock())   // if the layer is not locked
//            {
//                ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
//                string output = textinput->getTextString();
//                layers[selectedUILayer].setX_(ofToInt(output));
//            }
//            else    // if the layer is locked we have to get the old value
//            {
//                ofxUITextInput *xValue = (ofxUITextInput*) gui[selectedUILayer]->getWidget("posX");
//                xValue->setTextString(ofToString(layers[selectedUILayer].getOrigin_().x));
//            }
//        }
//        if(name == "posY" && selectedUILayer >= 0){
//            if( !layers[selectedUILayer].getLock()) // if the layer is not locked
//            {
//                ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
//                string output = textinput->getTextString();
//                layers[selectedUILayer].setY_(ofToInt(output));
//            }
//            else{
//                ofxUITextInput *yValue = (ofxUITextInput*) gui[selectedUILayer]->getWidget("posY");
//                yValue->setTextString(ofToString(layers[selectedUILayer].getOrigin_().y));
//            }
//        }
//        if(name == "rot" && selectedUILayer >= 0){
//            if(!layers[selectedUILayer].getLock()) // if the layer is not locked
//            {
//                ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
//                string output = textinput->getTextString();
//                layers[selectedUILayer].setAngle_(ofToInt(output));
//            }
//            else
//            {
//                ofxUITextInput *rValue = (ofxUITextInput*) gui[selectedUILayer]->getWidget("rot");
//                rValue->setTextString(ofToString(layers[selectedUILayer].getAngle_()));
//            }
//        }
//        if(name == "lock" && selectedUILayer >= 0)
//        {
//            ofxUIMultiImageToggle *toggleButton = (ofxUIMultiImageToggle*) e.widget;
//            bool lockLay = toggleButton->getValue();
//            layers[selectedUILayer].toggleLock();
//        }
//        if(name == "Opacity" && selectedUILayer >= 0)
//        {
//            if(!layers[selectedUILayer].getLock())
//            {
//                ofxUISlider *slider = (ofxUISlider *) e.getSlider();
//                layers[selectedUILayer].setAlpha(slider->getPercentValue() * 255);
//            }
//            else
//            {
//                ofxUISlider *yValue = (ofxUISlider*) gui[selectedUILayer]->getWidget("Opacity");
//                cout << "alpha: " << layers[selectedUILayer].getAlpha() / 255.f<< endl;
//
//                double v = (double)100.; // don't work dunno why ?!
//                yValue->setValue(v);
//                cout << "alpha: " << yValue->getValue()<< endl;
//            }
//        }
//
//        // -------- GLOBAL MENU ------------------------------------------------
//        if(name == "mouse")
//        {
//
//        }
//        if(name == "browseVideo")
//        {
//            ofxUIMultiImageToggle *toggleButton = (ofxUIMultiImageToggle*) e.widget;
//            bool browseImgButton = toggleButton->getValue();
//            if(browseImgButton){
//                FileB file;
//                string path = file.browseImg();
//                if(path != ""){
//                    addNewLay(2, path );
//                }
//                toggleButton->setValue(false);
//            }
//        }
//        if(name == "browseImg")
//        {
//            ofxUIMultiImageToggle *toggleButton = (ofxUIMultiImageToggle*) e.widget;
//            bool browseImgButton = toggleButton->getValue();
//            if(browseImgButton){
//                FileB file;
//                string path = file.browseImg();
//                if(path != ""){
//                    addNewLay(1, path );
//                }
//                toggleButton->setValue(false);
//            }
//        }
//        if(name == "warp")
//        {
//            cout << "warp mode" << endl;
//            ofxUIMultiImageToggle *toggleButton = (ofxUIMultiImageToggle*) e.widget;
//            bool warpButton = toggleButton->getValue();
//
//            if(!layers[selectedUILayer].locked)
//            {
//                if( *selectedLayer != 1 )
//                if( layers[*selectedLayer].isInWarpMode() )
//                {
//                    layers[selectedUILayer].disableWarper();
//                }
//
//                else
//                {
//                    layers[selectedUILayer].enableWarper();
//                }
//            }
//            if(warpButton)
//            {
//                ofxUIMultiImageToggle *toggleButton = (ofxUIMultiImageToggle*) globalMenu->getWidget("mouse");
//                toggleButton->setValue(false);
//            }
//            else{
//                ofxUIMultiImageToggle *mouse = (ofxUIMultiImageToggle*) globalMenu->getWidget("mouse");
//                // default button if none of them is choosen
//                if(mouse->getValue() == false)
//                {
//                    mouse->setValue(true);
//                }
//            }
//        }
//    }
//
//    void UI::exit(){
//        for(int i = 0; i < gui.size(); ++i){
//            delete gui[i];
//        }
//        delete globalMenu;
//    }
//
//    /* ************************************ */
//    /* ************* Layers *************** */
//    /* ************************************ */
//
//    void UI::setLayerUI(Layer &layer, int numLay){
//        gui.push_back(new ofxUISuperCanvas("Layer"+ofToString(numLay+1), OFX_UI_FONT_SMALL));
//
//        gui[numLay]->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
//        gui[numLay]->addMultiImageToggle("lock", "GUI/toggle.png", false);
//        gui[numLay]->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);
//
//        gui[numLay]->addSpacer();
//
//        // position
//        gui[numLay]->addTextArea("TEXT AREA", "Position: ", OFX_UI_FONT_SMALL);
//        gui[numLay]->addTextInput("posX", ofToString(layer.getOrigin_().x)+"",60,0)->setAutoClear(false);
//        gui[numLay]->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
//        gui[numLay]->addTextInput("posY", ofToString(layer.getOrigin_().y)+"",60,0)->setAutoClear(false);
//
//        gui[numLay]->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);
//
//        // rotation
//        gui[numLay]->addTextArea("TEXT AREA", "Rotation: ", OFX_UI_FONT_SMALL);
//        gui[numLay]->addTextInput("rot", ofToString(layer.getAngle_())+"",60,0)->setAutoClear(false);
//
//        // opacity
//        gui[numLay]->addIntSlider("Opacity", 0, 100, 100);
//
//        gui[numLay]->autoSizeToFitWidgets();
//        gui[numLay]->setPosition(0, (numLay) * 150 + 80 );
//
//        // color
//        setColorGui(*(gui[numLay]));
//
//        //gui[numLay-1]->setTheme(OFX_UI_THEME_MINLIME);
//        ofAddListener(gui[numLay]->newGUIEvent, this, &UI::UIguiEvent);
//    }
//
//    void UI::addLayerUI(Layer &layer, int selectedLay){
//        setLayerUI(layer, selectedLay);
//    }
//
//    void UI::updateLayerUI(int selectedLayer, Layer *a_layers){
//        if(gui[selectedLayer] != NULL && selectedLayer > -1) // if there is a layer
//        {
//            // position
//            ofxUITextInput *xValue = (ofxUITextInput*) gui[selectedLayer]->getWidget("posX");
//            xValue->setTextString(ofToString(a_layers[selectedLayer].getOrigin_().x));
//            ofxUITextInput *yValue = (ofxUITextInput*) gui[selectedLayer]->getWidget("posY");
//            yValue->setTextString(ofToString(a_layers[selectedLayer].getOrigin_().y));
//
//            // rotation
//            ofxUITextInput *rValue = (ofxUITextInput*) gui[selectedLayer]->getWidget("rot");
//            rValue->setTextString(ofToString(a_layers[selectedLayer].getAngle_()));
//
//            // opacity
//        }
//    }
//
//    void UI::deleteLayerUI(int layerToDel){
//        delete gui[layerToDel];
//        gui[layerToDel] = NULL;
//        gui.erase(gui.begin()+layerToDel);
//    }
//
//    /* Masks */
//
//    /* Mold */
//
//    /* Global Menu */
//    void UI::setGlobalMenu(){
//        globalMenu = new ofxUICanvas();
//        globalMenu->addMultiImageToggle("mouse", "GUI/toggle.png", true);
//        // button new layer a rajouter
//        globalMenu->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
//        globalMenu->addMultiImageToggle("browseVideo", "GUI/toggle.png", false);
//        globalMenu->addMultiImageToggle("browseImg", "GUI/toggle.png", false);
//        globalMenu->addMultiImageToggle("warp", "GUI/toggle.png", false);
//
//        globalMenu->autoSizeToFitWidgets();
//        globalMenu->setPosition(10, 0 );
//
//        ofAddListener(globalMenu->newGUIEvent, this, &UI::UIguiEvent);
//    }
