#include "RectAlphaBlendMask.h"

RectAlphaBlendMask::RectAlphaBlendMask(ofPoint R): RectMask(R)
{
    width = 100;
    height= 50;
    param= 0;
}

RectAlphaBlendMask::~RectAlphaBlendMask(){}

void RectAlphaBlendMask::setWidth(int w){
    width = w;
}

void RectAlphaBlendMask::changeParam(string s){

    if( s == "up" )
        ++param;
    if( s == "down" )
        --param;

    if( s == "reset" )
        param= 0;
}

int RectAlphaBlendMask::calcAlpha(int t){
    float tmp= exp((param + t-getWidth_()/2) / (0.5 * (width/2))); //getWidth au lieu de 50
    tmp = 255* tmp;
    int r= (int) tmp;

    return (int) tmp;
}

void RectAlphaBlendMask::drawPreview(){
    ofPushStyle();
        int temp = width/2;
        for(int i=0; i<=temp; ++i){
            ofSetColor(255,255,255, calcAlpha(i));
            img.drawSubsection( OM.x + temp - i-1, OM.y, 1, height,temp-i-1,0);
            ofSetColor(255,255,255, calcAlpha(i));
            img.drawSubsection( OM.x + temp + i, OM.y , 1, height,temp+i,0);
        }
    ofPopStyle();
}

void RectAlphaBlendMask::drawContent_(){
    ofPushStyle();
        int temp = width/2;
        for(int i=0; i<temp; ++i){
            ofSetColor(255,255,255, calcAlpha(i));
            img.drawSubsection( OM.x + temp - i-1+240, OM.y, 1, height,temp-i-1,0);
            ofSetColor(255,255,255, calcAlpha(i));
            img.drawSubsection( OM.x + temp + i+240, OM.y , 1, height,temp+i,0);
        }
    ofPopStyle();
}
