#ifndef MASK_H
#define MASK_H

#include "ofPoint.h"
#include "ofImage.h"
#include "Reshapable.h"

/********************************************//**
 *  \file Mask.h
 *  \brief Mask
 *
 *
 ***********************************************/

class Mask_ : public Reshapable
{

    private :
        //virtual int getPositionX();
        //virtual int getPositionY();
        //virtual void setPositionX(int other_position_x);
        //virtual void setPositionY(int other_position_y);
        //virtual int getWidth() =0;
        //virtual int getHeight() =0;
        //int position_x;
        //int position_y;

    public:
        Mask_(ofPoint R);
        //Mask_();
        virtual ~Mask_();

        virtual void drawFrame() =0;
        virtual void drawContent_() =0;
        virtual void drawPreview() =0;

        //virtual int getAngle();
        //virtual int getOpacity();
        //virtual void setAngle(int other_angle);
        //virtual void setOpacity(int other_opacity);

       /* virtual void setOrigin( float x, float y ){
            origin.set(x, y);
        }
        virtual ofPoint getOrigin( ){
            return origin;
        }

        virtual bool inside(int x, int y) =0;


*/
        virtual void changeParam(string s) =0;
        virtual void updateImg(ofPoint p) =0;
        virtual void updateOM( ofPoint o ){
            OM.set(getOrigin_() - o);
        }


        ofImage img;

    public:
        ofVec2f OM;
        //effect
        //int opacity; //De 0 à 255
};

#endif
