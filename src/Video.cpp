#include "Video.h"

Video::Video(string path, char myType){
    myVideo.loadMovie(path);
    type=myType;

    //std::cout << "Video loaded" << std::endl;
    cerr << "Video! >";
}

Video::~Video(){}

void Video::draw(ofPoint p, float width, float height){
    myVideo.draw(p.x,p.y,width, height);
}

float Video::getWidth(){
    return myVideo.getWidth();
}

float Video::getHeight(){
    return myVideo.getHeight();
}

void Video::play(){
    myVideo.play();
}


void Video::pause(bool pauseState){
    myVideo.setPaused(pauseState);
}

void Video::stop(){
    myVideo.stop();
}

void Video::setVolume(float volume){
    myVideo.setVolume(volume);
}

void Video::update(){
    myVideo.update();
}
