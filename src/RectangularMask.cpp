#include "RectangularMask.h"

/*****************************************/
/* CLASSE CONDAMNEE ( cf RectMask )     */
/****************************************/

RectangularMask::RectangularMask():Mask_(ofPoint())
{
    width = 0;
    height = 0;
    isSelected = false;

    cout << "Attention RectangularMask est obsolete" << endl;
}

RectangularMask::~RectangularMask()
{
    //dtor
}

int RectangularMask::getWidth()
{
    return width;
}

int RectangularMask::getHeight(){

    return height;

}

/*bool RectMask::getIsselected()
{
    return isSelected;
}


void RectMask::setWidth(int other_width)
{
    this->width=other_width;
}

void RectMask::setHeight(int other_height)
{
    this->height=other_height;
}*/

void RectangularMask::drawFrame(){

    ofSetColor(255,0,0);
    ofNoFill();
    ofRect(getOrigin_(), width, height);

    ofSetColor(255);

}

void RectangularMask::drawContent_()
{
    ofSetColor(255,255,255);
    img.draw( origin.x + 240, origin.y );
    /*
    switch (reply)
    {
    case "n": ofSetColor(255,255,255);
        break;
    case "o": ofSetColor(255,255,255, opacity);
        break;
    case "b": ofSetColor(0,0,0);
        break;
    default: ofSetColor(255,255,255);
        break;
    }*/
}


void RectangularMask::setIsselectedOff()
{
    this->isSelected=false;
}

void RectangularMask::setIsselectedOn()
{
    this->isSelected=true;
}

