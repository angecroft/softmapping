#include "Layer.h"

//--------------------------------------------------------------
Layer::Layer(char type, string path, float x, float y, ofPoint R): Reshapable(R, x, y)
{

    if( "" == path )
        path= "of_500x328.jpg";

    switch ( type ){
        case 1:
            src= new Image(path, type);
            break;

        case 2:
            src= new Video(path, type);
            break;

        default: cerr << "Source type unknown !" << endl;
    }
    angle = 0;

    width= src->getWidth();
    height= src->getHeight();
    contWidth= width;
    contHeight= height;

    anchor.set(width/2, height/2);
    //origin.set(x,y,0);

    visible= true;
    locked= false;
    alpha= 255;
   // anchor.set( origin.x + width/2, origin.y + height/2 );
    //OA.set(origin.x - anchor.x, origin.y - anchor.y);
    initWarp_();
    cerr << "New Layer" << endl;
    anch = true;
    resizeZone= true;
}

//--------------------------------------------------------------
void Layer::drawContent_(){

    ofPushMatrix();
    ofTranslate( -ref );
    if ( visible ){
            warper.begin();
            /*ofTranslate( -70, -70 );

            ofTranslate( anchor );
            ofRotateZ(angle);
            ofTranslate( OA.x, OA.y );*/

            ofPushStyle();
                ofSetColor(255,255,255,getAlpha());
                src->draw(ofPoint(0,0));//src->draw(origin);
            ofPopStyle();

            warper.end();
    }
    else{
            warper.begin();
            /*ofTranslate( -70, -70 );

            ofTranslate( anchor );
            ofRotateZ(angle);
            ofTranslate( OA.x, OA.y );*/

            ofPushStyle();
                ofSetColor(140);
                ofNoFill();
                ofSetLineWidth(1);
                ofRect( 0, 0, width, height );
            ofPopStyle();

            warper.end();
    }
    ofPopMatrix();
}


//--------------------------------------------------------------
int Layer::getAlpha(){
    return alpha;
}

//--------------------------------------------------------------
void Layer::setAlpha( int pInt ){
    alpha= pInt;
    if(pInt > 255)
        alpha= 255;
    if(pInt < 0)
        alpha= 0;
}

//--------------------------------------------------------------
void Layer::addAlpha(int i){
    setAlpha(getAlpha() + i);
}

//--------------------------------------------------------------
void Layer::subAlpha(int i){
    setAlpha(getAlpha() - i);
}

//--------------------------------------------------------------
bool Layer::getLock(){
    return locked;
}

void Layer::toggleLock(){
    locked = !locked;
}

//--------------------------------------------------------------
char Layer::getType(){
    return src->getType();
}




