#include "File.h"


//--------------------------------------------------------------
string FileB::browseImg(){

		//Open the Open File Dialog
		ofFileDialogResult openFileResult= ofSystemLoadDialog("Select a jpg or png");
		cout << "browse img" << endl;

		//Check if the user opened a file
		if (openFileResult.bSuccess){

			ofLogVerbose("User selected a file");

			//We have a file, check it and process it
			ofLogVerbose("getName(): "  + openFileResult.getName());
            ofLogVerbose("getPath(): "  + openFileResult.getPath());

            ofFile file (openFileResult.getPath());

            if (file.exists()){

                ofLogVerbose("The file exists - now checking the type via file extension");
                string fileExtension = ofToUpper(file.getExtension());

                //We only want images
                if (fileExtension == "JPG" || fileExtension == "PNG" || fileExtension == "MOV") {

                    //Save the file extension to use when we save out
                    originalFileExtension = fileExtension;

                    //Load the selected image
                    /*ofImage image;
                    image.loadImage(openFileResult.getPath());
                    *img= image;*/

                    return openFileResult.getPath();

                }
            }
		}
		else
			ofLogVerbose("User hit cancel");

		return "";
}
    /*
void FileB::SaveTemp(){

		ofFileDialogResult saveFileResult = ofSystemSaveDialog(ofGetTimestampString() + "." + ofToLower(originalFileExtension), "Save your file");
		if (saveFileResult.bSuccess){
			processedImages[0].saveImage(saveFileResult.filePath);
		}
}
    */


