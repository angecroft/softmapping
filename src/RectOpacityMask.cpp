#include "RectOpacityMask.h"

RectOpacityMask::RectOpacityMask(ofPoint R): RectMask(R)
{
    //ctor
    width= 100;
    height= 50;
    opacity= 255;
}

RectOpacityMask::~RectOpacityMask()
{
    //dtor
}

void RectOpacityMask::changeParam(string s){

    if( s == "up" )
        opacity += 5;
    if( s == "down" )
        opacity -= 5;

    if( s == "reset" ){
        opacity= 255;
    }
}

void RectOpacityMask::drawPreview(){
    ofPushStyle();
        ofEnableAlphaBlending();
        //ofSetColor(255,255,255, 25);
        ofSetColor(255,255,255, opacity);
        img.draw(OM);
        ofDisableAlphaBlending();
    ofPopStyle();
}

void RectOpacityMask::drawContent_(){
    //ofPushStyle();
        ofEnableAlphaBlending();
        //ofSetColor(255,255,255, 25);
        ofSetColor(255,255,255, opacity);
        img.draw(origin.x + 240, origin.y);
        ofDisableAlphaBlending();
    //ofPopStyle();
}


