#ifndef RECTALPHABLENDMASK_H
#define RECTALPHABLENDMASK_H

#include <math.h>
#include "RectMask.h"


class RectAlphaBlendMask : public RectMask
{
    public:
        RectAlphaBlendMask(ofPoint R);
        ~RectAlphaBlendMask();
        void drawPreview();
        void drawContent_();
        void setWidth(int w);
        void changeParam(string s);
        void updateImg(ofPoint p){
            img.grabScreen(p.x, p.y, width, height );
        }
    protected:
    private:
        //int width;

        int calcAlpha(int t);
        //int alphaOut(int t);
        int param;
};

#endif // RECTALPHABLENDMASK_H
