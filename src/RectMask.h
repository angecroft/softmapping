#ifndef RECTMASK_H
#define RECTMASK_H

#include "Mask.h"

/********************************************//**
 *  \file RectMask.h
 *  \brief Rectangle Mask
 *
 *  Inherited from Mask. Rectangle-shaped mask
 ***********************************************/


class RectMask : public Mask_
{
    private:
        bool getIsselected();
        void setIsselectedOff();
        void setIsselectedOn();
        bool isSelected;

    public:
        RectMask(ofPoint R);
        virtual ~RectMask();
        virtual void drawPreview();
        virtual void drawFrame();
        virtual void drawContent_();
        void changeParam(string s);

        int getWidth();
        int getHeight();

        void setWidth(int other_width);
        void setHeight(int other_height);

        virtual bool inside(int x, int y);

        void updateImg(ofPoint p){
            img.grabScreen(p.x, p.y, width, height );
        }


};

#endif
