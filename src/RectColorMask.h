#ifndef RECTCOLORMASK_H
#define RECTCOLORMASK_H

#include "RectMask.h"

class RectColorMask : public RectMask
{
    public:
        RectColorMask(ofPoint R);
        virtual ~RectColorMask();
        void setR(int red);
        void setG(int green);
        void setB(int blue);
        void drawPreview();
        void drawContent_();
        void changeParam(string s);
        void updateImg(ofPoint p){
            img.grabScreen(p.x, p.y, width, height );
        }
    protected:
    private:
        int r;
        int g;
        int b;
};

#endif // RECTCOLORMASK_H
