#include "LayWspace.h"

//--------------------------------------------------------------
/*void LayWspace::setup(){

}

//--------------------------------------------------------------
void LayWspace::update(){

    //states[currState].update();

}*/

//--------------------------------------------------------------
void LayWspace::draw(){
    ofSetColor(0,0,255);
    ofRect(origin,width,height);
    ofDrawBitmapString( "Layers", origin.x, origin.y-15);
    ofSetColor(255,255,255);
    ofLine(origin.x,origin.y+25,origin.x+width,origin.y+25);
    ofRect(origin.x+ width -10,origin.y + height -10,10,10);
}

//--------------------------------------------------------------
void LayWspace::leftClic(int x, int y ){

    if (insideMoveDiv(x, y))
        action = 'm'; //move ws
    else
        if(insideScaleDiv(x, y))
            action= 's'; //scale
        else
            action = 'n'; //none

}


bool LayWspace::insideMoveDiv(int x, int y){
    if(     x > origin.x && x < origin.x + width
        &&  y > origin.y && y < origin.y + 25 )
            return true;
    return false;

}

bool LayWspace::insideScaleDiv(int x, int y){
    if(     x > origin.x + width -10 && x < origin.x + width
        &&  y > origin.y + height -10  && y < origin.y + height )
            return true;
    return false;

}

//--------------------------------------------------------------
void LayWspace::dragLeft(int x, int y ){

    //char == 'movews'
    switch (action){
        case 'm' : move_ws(x-width/2, y-15);
                    break;
        case 's' : scale_ws(x, y);
                    break;

        case 'n' : break;

        default: cout << "A finir -> LayWspace"<< endl;
    }

}
/*
//--------------------------------------------------------------
void LayWspace::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void LayWspace::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void LayWspace::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void LayWspace::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void LayWspace::windowResized(int w, int h){

}

//--------------------------------------------------------------
void LayWspace::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void LayWspace::dragEvent(ofDragInfo dragInfo){

}*/

