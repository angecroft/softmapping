#include "MapIn.h"
#include "LayWspace.h"
#include "CompWspace.h"
#include "File.h"
#include "RectMold.h"
#include "Video.h"

/*
 * Layer     -  NEW(addNewLay)      -> DEL(supprLay)
 * Workspace -  NEW(setup)          -> dtor ?
 * RectMold  -  NEW(mousePressed)   -> DEL(keyPressed)
 */
bool menumold= true;
//--------------------------------------------------------------
void MapIn::setup(){

    gridX= 70;
    gridY= 70;
    grid = false;
    infoKeys = false;
    keydown= '?';

    nb_Lay= 0;
    selectWS = -1;
    selectLay = -1;
    select = NULL;
    drag= NONE;

    OV.set(0, 0, -1);

    nb_WS= 2;
    a_wspaces= new WorkSpace*[nb_WS];
    a_wspaces[0]= new CompWspace(260, 100, 700, 500) ;
    a_wspaces[1]= new LayWspace(20, 90, 200, 400);

    selectedGui = 0;
    layerDeleted = -1;

    ext= NULL;

    addNewLay(1, "of_500x328.jpg"); //Layer avec une source 'i'mage
    //addNewLay('j', "imac_logo.png"); //Exemple, � supprimer
    //addNewLay(2, "ipod.mov"); //DON'T WORK

    //gui

    // # gui->init();
    setGUIMenu();
    setGUIMolds();
    updateMold = false;
}
//------------------------------------------------------------
//------ GUI -------------------------------------------------
//------------------------------------------------------------
void MapIn::setColorGui(ofxUISuperCanvas& gui,int type = 0)
{
    if(type == 0){
        ofxUIColor cb = ofxUIColor( 70, 70, 70, 180 ); // background
        ofxUIColor co = ofxUIColor( 0, 0, 0, 255 ); // ?
        ofxUIColor coh = ofxUIColor( 0, 0, 0, 255 ); // textArea:hover
        ofxUIColor cf = ofxUIColor( 120, 120, 120, 200 ); // lime color
        ofxUIColor cfh = ofxUIColor( 209, 227, 137, 255 ); // textArea:inside
        ofxUIColor cp = ofxUIColor( 0, 0, 0, 255 ); // ?
        ofxUIColor cpo = ofxUIColor( 0, 0, 0, 255 ); // ?
        gui.setUIColors( cb, co, coh, cf, cfh, cp, cpo );
    }
    else{
        ofxUIColor cb = ofxUIColor( 70, 70, 70, 180 ); // background
        ofxUIColor co = ofxUIColor( 0, 0, 0, 255 ); // ?
        ofxUIColor coh = ofxUIColor( 0, 0, 0, 255 ); // textArea:hover
        ofxUIColor cf = ofxUIColor( 41, 193, 51, 255 ); // lime color
        ofxUIColor cfh = ofxUIColor( 209, 227, 137, 255 ); // textArea:inside
        ofxUIColor cp = ofxUIColor( 0, 0, 0, 255 ); // ?
        ofxUIColor cpo = ofxUIColor( 0, 0, 0, 255 ); // ?
        gui.setUIColors( cb, co, coh, cf, cfh, cp, cpo );
    }
}

void MapIn::updateColorSelectedGUI(){
    for(int j=0; j<gui.size(); ++j)
    {
        setColorGui(*(gui[j]));
    }
    if(selectLay>-1){
        setColorGui(*(gui[selectLay]),1);
        selectedGui = selectLay;
    }
}

//--------------------------------------------------------------
void MapIn::update(){
    updateColorSelectedGUI();
    //updateGUILayer();
    //deleteGUILayer();
    updatePosGUILayer();
    //updateGUIMold();
    if(updateMold) {
        if( menumold )
            upgradeGUIMold();
        updateMold = false;
    }

    //cerr << selectLay << endl;
    /*for(int i= 0; i < nb_Lay; ++i)
        if( a_layers[i]->getType() == 2 ){
            Video* movie= (Video*) a_layers[i]->src;
            movie->update();
        }*/
    //states[currState].update();
    //cout << "TL " << a_layers[0]->warper.getCorner(ofxGLWarper::TOP_LEFT) << endl;
    //cout << "OR " << a_layers[0]->origin << endl;
}


ofImage MapIn::moldOut(){

    ofImage out;

    a_wspaces[0]->draw_start();

        for( int i= 0; i < nb_Lay; ++i )
            a_layers[ layRank[i] ]->drawContent_();
        /*
        if( OV.z )
            a_layers[ layRank[nb_Lay-1] ]->drawContent_();
        */
    a_wspaces[0]->draw_end();

    if(select != NULL )
        out.grabScreen( select->getOrigin_().x+a_wspaces[0]->origin.x, select->getOrigin_().y+a_wspaces[0]->origin.y
                           , select->getWidth_(), select->getHeight_() );
    return out;

}

Video* MapIn::getVideo(){

    if( a_layers[ layRank[nb_Lay-1] ]->getType() != 2 )
        return NULL;

   return (Video*) a_layers[ layRank[nb_Lay-1] ]->src;
}

//--------------------------------------------------------------
void MapIn::draw(){

    //ofBackground(100);
    ofPushStyle();
    ofBackgroundGradient(ofColor(255, 255, 255), ofColor(0, 0, 0), OF_GRADIENT_CIRCULAR);

    ofCircle(400, 300, 20);
    ofDrawBitmapString( "MapIn", 940, 25);

    if( infoKeys ){
        a_wspaces[0]->scale_ws(760, 600);
        ofDrawBitmapString( "z:  New file...", 780, 120);
        ofDrawBitmapString( "m:  New mold", 780, 140);
        ofDrawBitmapString( "w:  Warp mode", 780, 160);
        ofDrawBitmapString( "x:  Delete layer", 780, 180);
        ofDrawBitmapString( "l:  Lock layer", 780, 200);
        ofDrawBitmapString( "v:  Visibility", 780, 220);
        ofDrawBitmapString( "r:  Rotate (-5�)", 780, 240);
        ofDrawBitmapString( "t:  Rotate (+5�)", 780, 260);
        ofDrawBitmapString( "b:  Backward", 780, 280);
        ofDrawBitmapString( "n:  Forward", 780, 300);
        ofDrawBitmapString( "g:  Grid", 780, 320);
        ofDrawBitmapString( "a:  Snap to grid", 780, 340);
        ofDrawBitmapString( "i:  Keyboard shortcuts", 780, 360);
    }
    else
        a_wspaces[0]->scale_ws(960, 600);

    //Compositing workspace first (behind)
    a_wspaces[0]->draw_start();       //------------------------- {

        if(grid)
            drawGrid(a_wspaces[0]->width, a_wspaces[0]->height);

    //------------------------------
        /*ofFill();
        ofSetColor(220);
        for(int x = 0; x < 1000; x += 20){
            for(int y = 0; y < 1000; y += 20){
                ofCircle(x, y, sin((x + y) / 100.0f + ofGetElapsedTimef()) * 5.0f);
            }
        }*/
    //------------------------------
        for( int i= 0; i < nb_Lay; ++i ){
            a_layers[ layRank[i] ]->drawContent_();
            if( layRank[i] == selectLay )
                a_layers[ layRank[i] ]->drawSelection_();
        }


        if(select != NULL)
            select->drawContent_();

        // end of compositing
        a_wspaces[0]->draw_end();                //------------------------- }

        ofSetColor(255, 255, 255);

        if( selectLay != -1 ){
            if( a_layers[selectLay]->isInWarpMode() )
                ofDrawBitmapString( "WARP MODE", 600, 25+15);

            if( a_layers[selectLay]->locked )
                ofDrawBitmapString( "LAYER LOCKED", 600, 60);
        }
        //ofDrawBitmapString( "Page down >> opacity ", 600, 75);

    ofPopStyle();

}

//--------------------------------------------------------------
void MapIn::addNewLay(char type, string path){
    cout << "new lay !" << endl;
    if( "" != path ){
        a_layers.push_back( new Layer(type, path, 70, 70, ofPoint(260,100)) );
        layRank.push_back( nb_Lay );
        selectLay= nb_Lay;
        cout << "Lay " << nb_Lay << " created !" << endl;
        // # gui->add(a_layers[nb_Lay]);
        setGUILayer(*(a_layers[nb_Lay]), nb_Lay+1);
        ++nb_Lay;

    }
    else
        cerr << "none";
}

//--------------------------------------------------------------
void MapIn::supprLay(){

    delete a_layers[selectLay];
    a_layers.erase(a_layers.begin()+selectLay);

    for( int k= 0; k < nb_Lay; ++k)
        if( layRank[k] > selectLay )
            --layRank[k];

    int i= 0;
    while( selectLay != layRank[i])
        ++i;
    layRank.erase(layRank.begin()+i);

    layerDeleted = selectLay;

    --nb_Lay;
    --selectLay;

    deleteGUILayer();
}

int MapIn::currWorkspace( float x, float y){

    for(int i= nb_WS-1; i >= 0; --i)
        if(     x > a_wspaces[i]->origin.x && x < (a_wspaces[i]->origin.x + a_wspaces[i]->width)
            &&  y > a_wspaces[i]->origin.y && y < (a_wspaces[i]->origin.y + a_wspaces[i]->height) )
                return i;
    return -1;
}

int MapIn::currLayer( float x, float y, int ws){

    if( !ws ) //in Compositing Workspace
        for(int k = nb_Lay-1; k >= 0; --k){

            int i= layRank[k];

            if( a_layers[i]->inside_( ofPoint(x, y) ) )
                if ( a_layers[i]->visible )
                    return i;
                else
                    continue;
        }
    else
        if( 1 == ws ){ //in Layers Workspace

            ofPoint pt( x-a_wspaces[1]->origin.x , y-a_wspaces[1]->origin.y );

            for( int i= nb_Lay-1; i >= 0 ; --i ){

                ofRectangle Rtmp(5,30+(nb_Lay-i-1)*55,90,50); //Caution ! Must match with Draw
                if( Rtmp.inside(pt) )
                    return layRank[i];
            }

            return selectLay;
        }
    return -1;
}

//--------------------------------------------------------------
void MapIn::toFront(int layNum){

    if( layRank[nb_Lay-1] != layNum ){//if it's not already the first Layer

        int i = nb_Lay-2;
        while( layRank[i] != layNum )
            --i;

        layRank.erase( layRank.begin() + i);
        layRank.push_back(layNum);
    }
}

//--------------------------------------------------------------
void MapIn::toBack(int layNum){

    if( layRank[0] != layNum ){//if it's not already the last Layer
        int i = 1;
        while( layRank[i] != layNum )
            ++i;

        layRank.erase( layRank.begin() + i);
        layRank.insert( layRank.begin(), layNum);
    }
}

//--------------------------------------------------------------
void MapIn::backward(int layNum){

    if( layRank[0] != layNum ){//if it's not already the last Layer

        int i = 1;
        while( layRank[i] != layNum )
            ++i;

        int prev = layRank[i-1];
        layRank[i-1]= layNum;
        layRank[i]= prev;
    }
}

//--------------------------------------------------------------
void MapIn::forward(int layNum){

    if( layRank[nb_Lay-1] != layNum ){//if it's not already the first Layer

        int i = nb_Lay-2;
        while( layRank[i] != layNum )
            --i;

        int next = layRank[i+1];
        layRank[i+1]= layNum;
        layRank[i]= next;
    }
}
//------------------------------------------------------------
//------ GUI -------------------------------------------------
//------------------------------------------------------------

//------ Layers ----------------------------------------------

void MapIn::setGUILayer(Layer &layer, int numLay)
{
    gui.push_back(new ofxUISuperCanvas("Layer"+ofToString(numLay), OFX_UI_FONT_SMALL));

    gui[numLay-1]->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);
    gui[numLay-1]->addSpacer();

    // lock
    gui[numLay-1]->addMultiImageToggle("lock", "GUI/lock.png", false);
    gui[numLay-1]->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);

    // visibility
    gui[numLay-1]->addMultiImageToggle("visibility", "GUI/visibility.png", true);
    gui[numLay-1]->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);
    gui[numLay-1]->addSpacer();

    // position
    gui[numLay-1]->addTextArea("TEXT AREA", "Position: ", OFX_UI_FONT_SMALL);
    gui[numLay-1]->addTextInput("posX", ofToString(layer.getOrigin_().x)+"",40,0)->setAutoClear(false);
    gui[numLay-1]->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
    gui[numLay-1]->addTextInput("posY", ofToString(layer.getOrigin_().y)+"",40,0)->setAutoClear(false);
    gui[numLay-1]->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);

    // rotation
    gui[numLay-1]->addTextArea("TEXT AREA", "Rotation: ", OFX_UI_FONT_SMALL);
    gui[numLay-1]->addTextInput("rot", ofToString(layer.getAngle_())+"",40,0)->setAutoClear(false);

    // opacity
//    gui[numLay-1]->addIntSlider("Opacity", 0, 100, 100);

    // alpha
    gui[numLay-1]->addLabelButton("Alpha +", false, 90, 0);
    gui[numLay-1]->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
    gui[numLay-1]->addLabelButton("Alpha -", false, 90, 0);
    gui[numLay-1]->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);

    // backward & forward
    gui[numLay-1]->addLabelButton("Backward", false, 90, 0);
    gui[numLay-1]->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
    gui[numLay-1]->addLabelButton("Forward", false, 90, 0);
    gui[numLay-1]->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);

    // delete
    gui[numLay-1]->addLabelButton("Delete", false, 90, 0);

    // position & size of the GUI
    gui[numLay-1]->autoSizeToFitWidgets();
    int height = gui[numLay-1]->getRect()->getHeight() ;
    gui[numLay-1]->setPosition(0, (numLay-1) * (height + 10) + 40 );

    // color
    setColorGui(*(gui[numLay-1]));

    ofAddListener(gui[numLay-1]->newGUIEvent, this, &MapIn::guiEvent);
}

void MapIn::addGUILayer(Layer &layer, int selectLay){
    for(int i=0; i < gui.size(); ++i)
        gui[i]->setMinified(true);

    setGUILayer(layer, selectLay+1);
}

/*void MapIn::setGUIMenu()
{
    globalMenu = new ofxUICanvas();
    globalMenu->addMultiImageToggle("mouse", "GUI/toggle.png", true);
    // button new layer a rajouter
    globalMenu->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
    globalMenu->addMultiImageToggle("browseVideo", "GUI/toggle.png", false);
    globalMenu->addMultiImageToggle("browseImg", "GUI/toggle.png", false);
    globalMenu->addMultiImageToggle("warp", "GUI/toggle.png", false);


    globalMenu->autoSizeToFitWidgets();
    globalMenu->setPosition(10, 0 );

    ofAddListener(globalMenu->newGUIEvent, this, &MapIn::guiEvent);
}*/

/*void MapIn::exit()
{
    for(int i = 0; i < gui.size(); ++i){
        delete gui[i];
    }

    delete globalMenu;
}*/

void MapIn::updateGUILayer(){
    if(gui[selectLay] != NULL && selectLay > -1) // if there is a layer
    {
        // position
        ofxUITextInput *xValue = (ofxUITextInput*) gui[selectLay]->getWidget("posX");
        xValue->setTextString(ofToString(a_layers[selectLay]->getOrigin_().x));
        ofxUITextInput *yValue = (ofxUITextInput*) gui[selectLay]->getWidget("posY");
        yValue->setTextString(ofToString(a_layers[selectLay]->getOrigin_().y));

        // rotation
        ofxUITextInput *rValue = (ofxUITextInput*) gui[selectLay]->getWidget("rot");
        rValue->setTextString(ofToString(a_layers[selectLay]->getAngle_()));

        // lock
        ofxUIMultiImageToggle *lockButton = (ofxUIMultiImageToggle*) gui[selectLay]->getWidget("lock");
        lockButton->setValue(a_layers[selectLay]->getLock());

        // visibility
        ofxUIMultiImageToggle *visibilityButton = (ofxUIMultiImageToggle*) gui[selectLay]->getWidget("visibility");
        visibilityButton->setValue(a_layers[selectLay]->visible);
    }
}

void MapIn::updatePosGUILayer(){
    int height = 40;
    for(int j=0; j<gui.size(); ++j){

        if(j == 0){ gui[j]->setPosition(0, height ); }
        else{ gui[j]->setPosition(0, height ); }
        height += gui[j]->getRect()->getHeight() + 10 ;
    }
}

void MapIn::deleteGUILayer(){

    if(layerDeleted != -1){
        delete gui[layerDeleted];
        gui[layerDeleted] = NULL;
        gui.erase(gui.begin()+layerDeleted);
        layerDeleted = -1;
    }
}

//------ Mold Menu -----------------------------------------
void MapIn::updateGUIMold(){
    if(select!=NULL) // if there is a mold
    {
        // position
        ofxUITextInput *xValueMoldPos = (ofxUITextInput*) MoldMenu->getWidget("posMoldX");
        xValueMoldPos->setTextString(ofToString(select->getOrigin_().x));
        ofxUITextInput *yValueMoldPos = (ofxUITextInput*) MoldMenu->getWidget("posMoldY");
        yValueMoldPos->setTextString(ofToString(select->getOrigin_().y));

        // size
        ofxUITextInput *xValueMoldSize = (ofxUITextInput*) MoldMenu->getWidget("sizeMoldX");
        xValueMoldSize->setTextString(ofToString(select->getWidth_()));
        ofxUITextInput *yValueMoldSize = (ofxUITextInput*) MoldMenu->getWidget("sizeMoldY");
        yValueMoldSize->setTextString(ofToString(select->getHeight_()));

    }
}

void MapIn::upgradeGUIMold(){
        delete MoldMenu;
        setGUIMolds();
}

void MapIn::setGUIMolds(){
    MoldMenu = new ofxUICanvas();

    MoldMenu->setWidgetPosition(OFX_UI_WIDGET_POSITION_LEFT);
    MoldMenu->addTextArea("TEXT AREA", "Mold ", OFX_UI_FONT_MEDIUM);


    MoldMenu->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
    if(select==NULL){
        MoldMenu->addLabelButton("Add Mold", false, 80, 0);
    }
    if(select!=NULL){
        MoldMenu->addLabelButton("Delete Mold", false, 80, 0);
    }

    MoldMenu->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);
    MoldMenu->addSpacer();
    if(select!=NULL){
        MoldMenu->addTextArea("TEXT AREA", "Position :", OFX_UI_FONT_SMALL);
        MoldMenu->addTextInput("posMoldX", ofToString(select->getOrigin_().x)+"",60,0)->setAutoClear(false);

        MoldMenu->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
        MoldMenu->addTextInput("posMoldY", ofToString(select->getOrigin_().y)+"",60,0)->setAutoClear(false);

        MoldMenu->setWidgetPosition(OFX_UI_WIDGET_POSITION_DOWN);
        MoldMenu->addTextArea("TEXT AREA", "Size :", OFX_UI_FONT_SMALL);
        MoldMenu->addTextInput("sizeMoldX", ofToString(select->getWidth_())+"",60,0)->setAutoClear(false);

        MoldMenu->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
        MoldMenu->addTextInput("sizeMoldY", ofToString(select->getHeight_())+"",60,0)->setAutoClear(false);
    }
    MoldMenu->autoSizeToFitWidgets();
    MoldMenu->setPosition(260, 603 );

    ofAddListener(MoldMenu->newGUIEvent, this, &MapIn::guiEvent);

}

//------ Global Menu -----------------------------------------
void MapIn::setGUIMenu(){
    globalMenu = new ofxUICanvas();

    globalMenu->setWidgetPosition(OFX_UI_WIDGET_POSITION_RIGHT);
    globalMenu->addMultiImageToggle("mouse", "GUI/mouse.png", true);
    globalMenu->addMultiImageToggle("warp", "GUI/warp.png", false);
    globalMenu->addMultiImageToggle("lockAll", "GUI/lock.png", false);
    globalMenu->addSpacer(1, 18);

    globalMenu->addMultiImageToggle("browseVideo", "GUI/addVideo.png", false);
    globalMenu->addMultiImageToggle("browseImg", "GUI/addImage.png", false);

    globalMenu->addSpacer(1, 18);
    globalMenu->addMultiImageToggle("grid", "GUI/grid.png", false);

    globalMenu->autoSizeToFitWidgets();
    globalMenu->setPosition(0, 0 );

    ofAddListener(globalMenu->newGUIEvent, this, &MapIn::guiEvent);
}

void MapIn::updateGlobalMenu(){
    // Grid
    ofxUIMultiImageToggle *GridButton = (ofxUIMultiImageToggle*) globalMenu->getWidget("grid");
    if( getGrid() ){ GridButton->setValue(true); }
    else{ GridButton->setValue(false); }

    //lockAll
    bool everyOneLock=false;
    for(int i=0; i<a_layers.size(); ++i){
        if( a_layers[i]->getLock() ){ everyOneLock = true; }
    }
    if(everyOneLock == false){
        ofxUIMultiImageToggle *LockButton = (ofxUIMultiImageToggle*) globalMenu->getWidget("lockAll");
        LockButton->setValue(false);
    }
}

//------ General functions
void MapIn::exit(){
    for(int i = 0; i < gui.size(); ++i){
        delete gui[i];
    }
    delete globalMenu;
}



void MapIn::guiEvent(ofxUIEventArgs &e)
{
    string name = e.getName();

    // -------- LAYERS -----------------------------------------------------
    if(name == "posX" && selectedGui >= 0)
    {
        if(!a_layers[selectedGui]->getLock())   // if the layer is not locked
        {
            ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
            string output = textinput->getTextString();
            a_layers[selectedGui]->setX_(ofToInt(output));
        }
        else    // if the layer is locked we have to get the old value
        {
            ofxUITextInput *xValue = (ofxUITextInput*) gui[selectedGui]->getWidget("posX");
            xValue->setTextString(ofToString(a_layers[selectedGui]->getOrigin_().x));
        }
    }
    if(name == "posY" && selectedGui >= 0)
    {
        if(!a_layers[selectedGui]->getLock()) // if the layer is not locked
        {
            ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
            string output = textinput->getTextString();
            a_layers[selectedGui]->setY_(ofToInt(output));
        }
        else
        {
            ofxUITextInput *yValue = (ofxUITextInput*) gui[selectedGui]->getWidget("posY");
            yValue->setTextString(ofToString(a_layers[selectedGui]->getOrigin_().y));
        }
    }
    if(name == "rot" && selectedGui >= 0)
    {
        if(!a_layers[selectedGui]->getLock()) // if the layer is not locked
        {
            ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
            string output = textinput->getTextString();
            a_layers[selectedGui]->setAngle_(ofToInt(output));
        }
        else
        {
            ofxUITextInput *rValue = (ofxUITextInput*) gui[selectedGui]->getWidget("rot");
            rValue->setTextString(ofToString(a_layers[selectedGui]->getAngle_()));
        }
    }
    if(name == "lock" && selectedGui >= 0) {
        ofxUIMultiImageToggle *toggleButton = (ofxUIMultiImageToggle*) e.widget;
        bool lockLay = toggleButton->getValue();
        a_layers[selectedGui]->toggleLock();
        updateGlobalMenu();
    }
    /*if(name == "Opacity" && selectedGui >= 0)
    {
        if(!a_layers[selectedGui]->getLock())
        {
            ofxUISlider *slider = (ofxUISlider *) e.getSlider();
            a_layers[selectedGui]->setAlpha(slider->getPercentValue() * 255);
        }
        else
        {
            ofxUISlider *yValue = (ofxUISlider*) gui[selectedGui]->getWidget("Opacity");
            cout << "alpha: " << a_layers[selectedGui]->getAlpha() / 255.f<< endl;

            double v = (double)100.; // don't work dunno why ?!
            yValue->setValue(v);
            cout << "alpha: " << yValue->getValue()<< endl;
        }
    }*/

    // Alpha
    if(name == "Alpha +" && selectedGui >= 0){
        if(!a_layers[selectedGui]->getLock()){
            a_layers[selectedGui]->addAlpha(5);
        }
    }
    if(name == "Alpha -" && selectedGui >= 0){
        if(!a_layers[selectedGui]->getLock()){
            a_layers[selectedGui]->subAlpha(5);
        }
    }

    // backward
    if(name == "Backward" && selectedGui >= 0){
        if(!a_layers[selectedGui]->getLock()){
            selectedGui = selectLay;
            backward(selectLay);
        }
    }
    // forward
    if(name == "Forward" && selectedGui >= 0){
        if(!a_layers[selectedGui]->getLock()){
            selectedGui = selectLay;
            forward(selectLay);
        }
    }

    //delete
    if(name == "Delete" && selectedGui >= 0){
        selectLay = selectedGui;
        supprLay();
    }

    if(name == "visibility" && selectedGui >= 0){
        a_layers[selectedGui]->visible = !a_layers[selectedGui]->visible;
    }

    // -------- GLOBAL MENU ------------------------------------------------
    //mode
    if(name == "mouse"){

    }
    if(name == "warp"){
        cout << "warp mode" << endl;
        ofxUIMultiImageToggle *toggleButton = (ofxUIMultiImageToggle*) e.widget;
        bool warpButton = toggleButton->getValue();
        cout <<"layer " <<selectLay << endl;

        if(!a_layers[selectedGui]->locked){
            if( selectLay != 1 )
            if( a_layers[selectLay]->isInWarpMode() )
            {
                a_layers[selectedGui]->disableWarper();
            }

            else{
                a_layers[selectedGui]->enableWarper();
            }
        }
        if(warpButton){
            ofxUIMultiImageToggle *toggleButton = (ofxUIMultiImageToggle*) globalMenu->getWidget("mouse");
            toggleButton->setValue(false);
        }
        else{
            ofxUIMultiImageToggle *mouse = (ofxUIMultiImageToggle*) globalMenu->getWidget("mouse");
            // default button if none of them is choosen
            if(mouse->getValue() == false)
            {
                mouse->setValue(true);
            }
        }
    }
    if(name == "lockAll"){
        ofxUIMultiImageToggle *lockAllButton = (ofxUIMultiImageToggle*) e.widget;
        bool lockAll = lockAllButton->getValue();
        for(int i=0; i<a_layers.size(); ++i){
            a_layers[i]->toggleLock();
            ofxUIMultiImageToggle *lockButton = (ofxUIMultiImageToggle*) gui[i]->getWidget("lock");
            lockButton->setValue(a_layers[i]->getLock());
        }
    }

    // files
    if(name == "browseVideo"){
        ofxUIMultiImageToggle *toggleButton = (ofxUIMultiImageToggle*) e.widget;
        bool browseImgButton = toggleButton->getValue();
        if(browseImgButton){
            FileB file;
            string path = file.browseImg();
            if(path != ""){
                addNewLay(2, path );
            }
            toggleButton->setValue(false);
        }
    }
    if(name == "browseImg"){
        ofxUIMultiImageToggle *toggleButton = (ofxUIMultiImageToggle*) e.widget;
        bool browseImgButton = toggleButton->getValue();
        if(browseImgButton){
            FileB file;
            string path = file.browseImg();
            if(path != ""){
                addNewLay(1, path );
            }
            toggleButton->setValue(false);
        }
    }

    //grid
    if(name == "grid"){
        toggleGrid();
    }

     // -------- MOLDS -----------------------------------------------------
     if(name == "posMoldX")
    {
        if(select!=NULL){
            ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
            string output = textinput->getTextString();
            select->setX_(ofToInt(output));
        }

    }

    if(name == "posMoldY")
    {
        if(select!=NULL){
            ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
            string output = textinput->getTextString();
            select->setY_(ofToInt(output));
        }

    }

    if(name == "sizeMoldX")
    {
        if(select!=NULL){
            ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
            string outputSize = textinput->getTextString();
            //select->resize__(260-select->getWidth_()+ofToInt(outputSize), 100-select->getHeight_());
            select->setDimensions(ofToInt(outputSize), select->getHeight_());
        }

    }

    if(name == "sizeMoldY")
    {
        if(select!=NULL){
            ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
            string outputSize = textinput->getTextString();
            //select->resize__(260-select->getWidth_(), 100-select->getHeight_()+ofToInt(outputSize));
            select->setDimensions(select->getWidth_(), ofToInt(outputSize));
        }

    }

    if(name == "Add Mold" && select==NULL)
    {
        if(select==NULL){
            cout << "RectMold created" << endl;
            select = new RectMold(ofPoint(260, 100));
            select->move_( 260-a_wspaces[0]->origin.x, 100-a_wspaces[0]->origin.y );
            select->resize__(360,200);
            updateMold = true;

        }
    }

    if(name == "Delete Mold" && select!=NULL)
    {
        select = NULL;
        updateMold = true;
    }
}


void MapIn::magnet(float xO, float yO){

    Layer* lay= a_layers[selectLay];
    float width= lay->getWidth_();
    float height= lay->getHeight_();

 	float prevNear = gridX * round(xO/gridX);
 	float nextNear = gridX * round( (xO+width)/gridX );

 	if( abs(prevNear - xO) <= abs( nextNear - (xO+width) ) )
 		lay->setX_( prevNear );
 	else
 		lay->setX_( nextNear - width );


    prevNear = gridY * round(yO/gridY);
 	nextNear = gridY * round( (yO+height)/gridY );

 	if( abs(prevNear - yO) <= abs( nextNear - (yO+height) ) )
 		lay->setY_( prevNear );
 	else
 		lay->setY_( nextNear - height );

 }


//--------- EVENT -------------------------------
void MapIn::keyPressed(int key){

    //ON LAYERS
    if( -1 != selectLay )
        switch( key ){
            case 'l':
                if (a_layers[selectLay]->isInWarpMode())
                    a_layers[selectLay]->disableWarper();
                a_layers[selectLay]->toggleLock();
                // #gui->update
                updateGlobalMenu();
                break;
            case 'v': // v
                a_layers[selectLay]->visible = !a_layers[selectLay]->visible;
                break;
            /*case 112: // p
                if ( a_layers[selectLay]->getType() == 2 ){
                    Video* v= (Video*) a_layers[selectLay]->src;
                    v->play();
                }
                break;*/
            case 114: // r
                a_layers[selectLay]->rotate_( -5 );
                break;
            case 'R':
                a_layers[selectLay]->rotate_( -90 );
                break;
            case 't':
                a_layers[selectLay]->rotate_( 5 );
                break;
            case 'T': // A
                a_layers[selectLay]->rotate_( 90 );
                break;
            case 98: // b
                backward(selectLay);
                break;
            case 110: // n
                forward(selectLay);
                break;
            case 'B':
                toBack(selectLay);
                break;
            case 'N':
                toFront(selectLay);
                break;
            case 120: // x
                supprLay();
                break;
            case OF_KEY_PAGE_UP:
                a_layers[selectLay]->addAlpha(3);
                break;
            case OF_KEY_PAGE_DOWN:
                a_layers[selectLay]->subAlpha(3);
                break;
            case 119: // w
                if( a_layers[selectLay]->locked )
                    break;
                if( a_layers[selectLay]->isInWarpMode() )
                    a_layers[selectLay]->disableWarper();
                else
                    a_layers[selectLay]->enableWarper();
                break;
            case 97: // a
                keydown= 'w';
                break;

            default: break;
        }

    switch( key ){
        case 122: // z
            {
                FileB file;
                addNewLay(1, file.browseImg() );
                //addGUILayer(*(a_layers[selectLay]),selectLay);
                break;
            }
        case 'q':
        case 'm':
            if( select != NULL ){
                delete select;
                select = NULL;
            }
            drag= RESIZE_MOLD;
            break;
        case 103: // g

            toggleGrid();
            updateGlobalMenu();
            break;
        case 'i':
                infoKeys = !infoKeys;
                break;
        default: break;
    }
}

//--------------------------------------------------------------
void MapIn::keyReleased(int key){
    keydown= '?';

}

//--------------------------------------------------------------
void MapIn::mouseMoved(int x, int y ){
}

//--------------------------------------------------------------
void MapIn::mouseDragged(int x, int y, int button){

    switch( selectWS ){

        case -1: //Out of Workspace
            break;

        case 0: //Compositing
            switch( drag ){
                case RESIZE_MOLD: // Size 'M'old
                    if(select !=NULL)
                        //stock History
                        select->resize__(x, y);
                        updateMold=true;
                        if ( a_layers[selectLay]->getType() == 2 )
                            updateOV();

                    break;

                case RESIZE_LAY: // Size 'M'old
                    a_layers[selectLay]->resize__(x, y);

                    break;

                case MOVE_MOLD: // 'm'ove 'm'old
                    if(select !=NULL){
                        //stock History
                        select->move_( x-(pressed.x - select->getOrigin_().x), y-(pressed.y - select->getOrigin_().y ) );
                        pressed.x = x;
                        pressed.y = y;
                        updateMold=true;
                        /*if ( a_layers[selectLay]->getType() == 2 )
                            updateOV();*/

                    }
                    break;

                case MOVE_LAY: // move 'l'ayer
                    if(selectLay != -1 && !a_layers[selectLay]->locked ){

                        int alpha = (int) trunc( a_layers[selectLay]->getAngle_() );

                        if( grid && keydown == 'w' && alpha%90 == 0 )
                            magnet( x-(pressed.x - a_layers[selectLay]->getOrigin_().x), y-(pressed.y - a_layers[selectLay]->getOrigin_().y ) );
                        else
                            a_layers[selectLay]->move_( x-(pressed.x - a_layers[selectLay]->getOrigin_().x), y-(pressed.y - a_layers[selectLay]->getOrigin_().y ) );

                        pressed.x = x;
                        pressed.y = y;
                        updateGUILayer();
                        if ( a_layers[selectLay]->getType() == 2 )
                            updateOV();
                    }
                    break;

                case MOVE_ANCHOR :
                    a_layers[selectLay]->changeAnchor__(x, y);
                    break;

                case RESIZE_GRID:
                    upGrid( (x - pressed.x)/5, (y - pressed.y)/5 );
                    pressed.x = x;
                    pressed.y = y;
                    break;

                case NONE: break;

                default :
                    cerr << "'" << drag << "' drag mode unknown !" << endl;
            }
            break; //WS

        case 1: //layer workspace
            a_wspaces[selectWS]->dragLeft(x, y);
            break; //WS

        default:
            cerr << "Workspace " << selectWS << " unknown !" << endl;

    }
}

void MapIn::updateOV(){

    if( select != NULL ){
        OV.set( a_layers[selectLay]->getOrigin_() - select->getOrigin_() );
        OV.z= 0;
    }
}

//--------------------------------------------------------------
void MapIn::mousePressed(int x, int y, int button){

        int curr= currWorkspace(x, y);

        pressed.set(x, y);

        for(int i=0; i<(gui.size()); ++i)
        {
            if(gui[i]->isHit(x,y))
            {
                selectedGui = i;
                selectLay = selectedGui;
            }
        }

        switch ( curr ){

            case -1 : break;
        //-------------------------------------------------------------
            case 0: // in Compositing

                selectWS= curr;

                switch( button ){

                    case 0: //Left Clic

                        if(drag == RESIZE_MOLD){

                            cout << "RectMold created" << endl;
                            select = new RectMold(ofPoint(260, 100));
                            select->move_( x-a_wspaces[0]->origin.x, y-a_wspaces[0]->origin.y );
                            break;
                        }

                        if(select != NULL){

                            if( select->insideScaleArea_(x, y) ){
                                drag = RESIZE_MOLD;
                                break;
                            }

                            if(select->inside_( pressed /*- a_wspaces[0]->origin*/ )){
                                drag = MOVE_MOLD; //'m'ove 'm'old
                                break;
                            }
                        }

                        if( -1 != selectLay ){
                            // ON ANCHOR ?
                            if( a_layers[selectLay]->insideAnchorArea__(x,y) ){
                                drag = MOVE_ANCHOR;
                                break;
                            }
                        }


                        selectLay= currLayer(x, y, curr);

                        if( -1 != selectLay ){
                            //WARP MODE ON ?
                            if( !a_layers[selectLay]->isInWarpMode() ){

                                if( a_layers[selectLay]->insideScaleArea_(x, y) ){
                                    drag = RESIZE_LAY;
                                    break;
                                }

                                if( -1 != selectLay )
                                    drag= MOVE_LAY; //move 'l'ayer

                            }
                        }
                        else
                            if( grid )
                                drag= RESIZE_GRID;
                        break;


                    case 2: //Right Clic
                        //a_wspaces[curr]->rightClic( x, y );
                        break;

                    default: break;

                }

                break;
        //-------------------------------------------------------------
            case 1: //Layers

                switch( button ){

                    case 0: //Left Clic
                        //SWITCH ( currButton )
                        selectWS= curr;
                        selectLay= currLayer(x, y, curr);
                        a_wspaces[curr]->leftClic( x, y );

                        break;

                    case 2: //Right Clic
                        //a_wspaces[curr]->rightClic( x, y );
                        break;

                    default: cerr << "Undetermined input (mouse)" << endl;
                }
                break;


            default:
                 break;
        }

        cout << "W." << selectWS << " L." << selectLay << endl;


}

//--------------------------------------------------------------
void MapIn::mouseReleased(int x, int y, int button){
    drag= NONE;

}

//--------------------------------------------------------------
void MapIn::windowResized(int w, int h){

}

//--------------------------------------------------------------
void MapIn::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void MapIn::dragEvent(ofDragInfo dragInfo){

}
