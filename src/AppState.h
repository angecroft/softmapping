#pragma once

#include "ofMain.h"
#include "WorkSpace.h"

/********************************************//**
 *  \file AppState.h
 *  \brief Application State
 *
 *  Interface of an Application step
 ***********************************************/


/*! \enum Actions
 *  Drag actions
 */
enum {
            DRAG_LAY, /**< enum value 1 */
            RESIZE_MOLD,
            MOVE_MOLD,
            MOVE_LAY,
            RESIZE_LAY,
            RESIZE_GRID,
            MOVE_ANCHOR,
            NONE
};

// ABSTRACT

class AppState : public ofBaseApp{

	public:

	    virtual ~AppState() { }

        virtual void draw() = 0;
		virtual int currWorkspace( float x, float y) = 0;

		void toggleGrid(){ grid= !grid;  }
		bool getGrid(){ return grid; }

		void upGrid( float x, float y ){
            upXGrid(x);
            upYGrid(y);
		}

		void upXGrid(float dx){
		    gridX+= dx;

		    if( gridX < 5 )
                gridX= 5;

            if( gridX > 350 )
                gridX= 350;
        }

		void upYGrid(float dy){
		    gridY+= dy;

		    if( gridY < 5 )
                gridY= 5;

            if( gridY > 350 )
                gridY= 350;
        }

		void drawGrid(float width, float height){

            float itw= width/gridX;
            float ith= height/gridY;

            ofPushStyle();
                ofSetColor(200);
                ofSetLineWidth(0.5);
                for( int i= 0; i < itw; ++i )
                    ofLine(i*gridX, 0, i*gridX, height);

                for( int i= 0; i < ith; ++i )
                    ofLine(0, i*gridY, width, i*gridY);
            ofPopStyle();
        }

		WorkSpace** a_wspaces;
		int nb_WS;
		int selectWS;
		bool grid;
		char keydown;
        float gridX;
        float gridY;
        string* ext;
        ofPoint OV;

        bool infoKeys;

		ofPoint pressed;

};
