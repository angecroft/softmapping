#pragma once

#include "ofVideoPlayer.h"
#include "Source.h"

/********************************************//**
 *  \file Video.h
 *  \brief Video Source
 *
 *
 ***********************************************/

class Video : public Source {
public:
    Video(string path, char myType);
    ~Video();

    virtual void draw(ofPoint p){ myVideo.draw(p.x,p.y); };
    float getWidth();
    float getHeight();
    /* ofPoint getOrigin(); --> pas dans Image.h ? */

    /* Méthodes hors documentation */

    void draw(ofPoint p, float width, float height);

    void play();
    void pause(bool pauseState);
    void stop();
    void setVolume(float volume); /* 0.0 < volume < 1.0 */
    void update();

private:
    ofVideoPlayer myVideo;
};
