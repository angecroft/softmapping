#include "CompWspace.h"
#include "ofMain.h"
//--------------------------------------------------------------
/*void LayWspace::setup(){

}

//--------------------------------------------------------------
void LayWspace::update(){

    //states[currState].update();

}*/

void CompWspace::drawViewportOutline(const ofRectangle & viewport){
	ofPushStyle();
	ofFill();
	ofSetColor(50);
	ofSetLineWidth(0);
	ofRect(viewport);
	ofNoFill();
	ofSetColor(25);
	ofSetLineWidth(1.0f);
	ofRect(viewport);
	ofPopStyle();
}

//--------------------------------------------------------------
void CompWspace::draw(){

        // 2d view
        drawViewportOutline(viewport2D);
        cout << "coucou !";
        // keep a copy of your viewport and transform matrices for later
        ofPushView();

        // tell OpenGL to change your viewport. note that your transform matrices will now need setting up
         ofViewport(viewport2D);
        // setup transform matrices for normal oF-style usage, i.e.
        //  0,0=left,top
        //  ofGetViewportWidth(),ofGetViewportHeight()=right,bottom
         ofSetupScreen();

}

//--------------------------------------------------------------
void CompWspace::leftClic(int x, int y ){

    if (insideMoveDiv(x, y))
        action = 'm'; //move ws
    else
        if(insideScaleDiv(x, y))
            action= 's'; //scale
        else
            action = 'n'; //none

}


bool CompWspace::insideMoveDiv(int x, int y){
    if(     x > origin.x && x < origin.x + width
        &&  y > origin.y && y < origin.y + 25 )
            return true;
    return false;

}

bool CompWspace::insideScaleDiv(int x, int y){
    if(     x > origin.x + width -10 && x < origin.x + width
        &&  y > origin.y + height -10  && y < origin.y + height )
            return true;
    return false;

}

//--------------------------------------------------------------
void CompWspace::dragLeft(int x, int y ){

    //char == 'movews'
    switch (action){
        case 'm' : move_ws(x-width/2, y-15);
                    break;
        case 's' : scale_ws(x, y);
                    break;

        case 'n' : break;

        default: cout << "A finir -> LayWspace"<< endl;
    }

}
/*
//--------------------------------------------------------------
void LayWspace::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void LayWspace::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void LayWspace::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void LayWspace::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void LayWspace::windowResized(int w, int h){

}

//--------------------------------------------------------------
void LayWspace::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void LayWspace::dragEvent(ofDragInfo dragInfo){

}*/

