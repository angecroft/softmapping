#include "RectColorMask.h"

RectColorMask::RectColorMask(ofPoint R): RectMask(R)
{
    r = 200;
    g = 50;
    b = 100;

    width= 100;
    height= 50;
}

RectColorMask::~RectColorMask()
{
    //dtor
}

void RectColorMask::changeParam(string s){

    if( s == "up" )
        setB(255);
    if( s == "down" )
        setR(255);

    if( s == "reset" ){
        setR(0);
        setG(0);
        setB(0);
    }
}

void RectColorMask::setR(int red){
    r = red;
}

void RectColorMask::setG(int green){
    g = green;
}

void RectColorMask::setB(int blue){
    b = blue;
}

void RectColorMask::drawPreview(){
    ofPushStyle();
        ofSetColor(r,g,b);
        img.draw(OM);
    ofPopStyle();
}

void RectColorMask::drawContent_(){
    ofPushStyle();
        ofSetColor(r,g,b);
        img.draw(origin);
    ofPopStyle();
}


