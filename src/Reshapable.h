#pragma once

#include "ofMain.h"
#include "Source.h"
#include "Image.h"
#include "Video.h"
#include "ofxGLWarper.h"

/********************************************//**
 *  \file Layer.h
 *  \brief Layer
 *
 *
 ***********************************************/

class Reshapable {

	public:

	    Reshapable( ofPoint R, float w, float h ){

            width= w;
            height= h;
            contWidth= w;
            contHeight= h;
            origin.set(0,0);
            ref= R;
            initWarp_();

            anchor.set(width/2, height/2);
            angle= 0;
            resizeArea = 10;
            warp= false;
            anch= false;
            resizeZone= false;

            cerr << "New Shape > ";
	    }

	    /*Reshapable( ){

	        ofxGLWarper warper;
	        initWarp(); //WIDHT - HEIGHT - WARPER - REF - ORIGIN
            ofPoint origin;
            ofPoint ref;
            ofPoint anchor;
            float width;
            float height;
            float angle;
            bool warp;

	    }*/

        ofPoint getOrigin_(){
            return origin;
        }

        float getWidth_(){
            return width;
        }

        float getHeight_(){
            return height;
        }

	    //SELECTABLE
//-----------------------------------------------------------------------------------------------------
	    int wise(ofPoint A, ofPoint B, ofPoint C){ // NONE
            if( (B.x-A.x)*(C.y-A.y) >= (B.y-A.y)*(C.x-A.x) )
                return 1; // Sens horaire
            return -1;  //Sens trigo
        }
//-----------------------------------------------------------------------------------------------------
	    bool inside_( ofPoint P ){ //WARPER

            ofPoint A= warper.getCorner(ofxGLWarper::TOP_LEFT);
            ofPoint B= warper.getCorner(ofxGLWarper::TOP_RIGHT);
            ofPoint C= warper.getCorner(ofxGLWarper::BOTTOM_RIGHT);
            ofPoint D= warper.getCorner(ofxGLWarper::BOTTOM_LEFT);

            if( wise(A, B, P) == wise( P, B, C) && wise(P, B, C) == wise(A, P, C) )
                return true;

            if( wise(A, C, P) == wise( P, C, D) && wise(P, C, D) == wise(A, P, D) )
                return true;

            return false;
        }


	    //DRAGGABLE
//-----------------------------------------------------------------------------------------------------
	    char getDragMode_(){ ; }
//-----------------------------------------------------------------------------------------------------
	    void move_(float x, float y){ // NONE
            setX_(x);
            setY_(y);
        }
//-----------------------------------------------------------------------------------------------------
        void setX_( float nX ){ // ORIGIN - ANCHOR - WARPER - REF
            //float nX= X - ref.x;
            float dx= nX - origin.x;
            origin.x= nX;
            anchor.x += dx;

            ofPoint o( warper.getCorner(ofxGLWarper::TOP_LEFT ) );
            o.x += dx;
            warper.setCorner(ofxGLWarper::TOP_LEFT, o);

            o= warper.getCorner(ofxGLWarper::TOP_RIGHT );
            o.x += dx;
            warper.setCorner(ofxGLWarper::TOP_RIGHT, o);

            o= warper.getCorner(ofxGLWarper::BOTTOM_LEFT );
            o.x += dx;
            warper.setCorner(ofxGLWarper::BOTTOM_LEFT, o);

            o= warper.getCorner(ofxGLWarper::BOTTOM_RIGHT );
            o.x += dx;
            warper.setCorner(ofxGLWarper::BOTTOM_RIGHT, o);
        }
//-----------------------------------------------------------------------------------------------------
        void setY_( float nY ){ // ORIGIN - ANCHOR - WARPER - REF
            //float nY= Y - ref.y;
            float dy= nY - origin.y;
            origin.y= nY;
            anchor.y += dy;

            ofPoint o( warper.getCorner(ofxGLWarper::TOP_LEFT ) );
            o.y += dy;
            warper.setCorner(ofxGLWarper::TOP_LEFT, o);

            o= warper.getCorner(ofxGLWarper::TOP_RIGHT );
            o.y += dy;
            warper.setCorner(ofxGLWarper::TOP_RIGHT, o);

            o= warper.getCorner(ofxGLWarper::BOTTOM_LEFT );
            o.y += dy;
            warper.setCorner(ofxGLWarper::BOTTOM_LEFT, o);

            o= warper.getCorner(ofxGLWarper::BOTTOM_RIGHT );
            o.y += dy;
            warper.setCorner(ofxGLWarper::BOTTOM_RIGHT, o);
        }


	    //DRAWABLE
//-----------------------------------------------------------------------------------------------------
	    virtual void drawContent_() =0;
        //virtual void drawContent_(ofPoint p) =0;
//-----------------------------------------------------------------------------------------------------
        void drawSelection_(){ // REF - WIDTH - HEIGHT - WARP - ANCHOR

            ofPushMatrix();
            ofTranslate( -ref );//ofTranslate( -260, -100 );
            warper.begin();
                ofPushStyle();

                if( !warp ){
                    ofSetColor(255,140,0);
                    ofNoFill();
                    ofSetLineWidth(2.5);
                    //ofRect( 0, 0, p.x - origin.x, p.y - origin.y);
                    ofRect( 0, 0, contWidth, contHeight );
                }
                else{
                    ofSetColor(255,0,140);
                    ofNoFill();
                    ofCircle(0, 0, 17);
                    ofCircle(contWidth, 0, 17);
                    ofCircle(0, contHeight, 17);
                    ofCircle(contWidth, contHeight, 17);
                }

                ofFill();
                if( resizeZone )
                    ofRect(contWidth -resizeArea, contHeight -resizeArea,resizeArea,resizeArea);

                ofPopStyle();
            warper.end();
            ofPopMatrix();

            if( anch ){
                ofPushStyle();
                ofNoFill();
                ofSetColor(255, 0, 0);
                ofCircle(anchor, 5);
                ofPopStyle();
            }
        }


        //WARPABLE
//-----------------------------------------------------------------------------------------------------
        void initWarp_(){ //
            warper.setup( 0,0, width, height );
            warper.setCornerSensibility(0.05);
            warper.activate();

            ofPoint TLPosition = origin + ref;//champ utile ?                           //Absolute
            ofPoint TRPosition = ofPoint(origin.x + width, origin.y) + ref;             //Absolute
            ofPoint BLPosition = ofPoint(origin.x, origin.y + height) + ref;            //Absolute
            ofPoint BRPosition = ofPoint(origin.x + width, origin.y + height) + ref;    //Absolute

            warper.setCorner(ofxGLWarper::TOP_LEFT, TLPosition);
            warper.setCorner(ofxGLWarper::TOP_RIGHT, TRPosition);
            warper.setCorner(ofxGLWarper::BOTTOM_LEFT, BLPosition);
            warper.setCorner(ofxGLWarper::BOTTOM_RIGHT, BRPosition);

            warper.deactivate();
        }
//-----------------------------------------------------------------------------------------------------
        void disableWarper(){
            warper.deactivate();
            warp = false;
        }
//-----------------------------------------------------------------------------------------------------
        void enableWarper(){
            warper.activate();
            warp = true;
        }
//-----------------------------------------------------------------------------------------------------
        bool isInWarpMode(){
            return warp;
        }


        //RESIZABLE
//-----------------------------------------------------------------------------------------------------
        virtual void resize__(float x, float y){ // WIDTH - HEIGHT - WARPER - REF

            float x_end= x - ref.x;
            float y_end= y - ref.y;
            //Use four points in order to have multidirectional selection
            //anchor.set( anchor.x + x_end - origin.x - width, anchor.y + y_end - origin.y - height );
            width = x_end - origin.x;
            height= y_end - origin.y;
            anchor.set( origin.x + width/2, origin.y + height/2);
            ofPoint TLPosition = origin + ref;//champ utile ?                           //Absolute
            ofPoint TRPosition = ofPoint(origin.x + width, origin.y) + ref;             //Absolute
            ofPoint BLPosition = ofPoint(origin.x, origin.y + height) + ref;            //Absolute
            ofPoint BRPosition = ofPoint(origin.x + width, origin.y + height) + ref;    //Absolute

            warper.setCorner(ofxGLWarper::TOP_LEFT, TLPosition);
            warper.setCorner(ofxGLWarper::TOP_RIGHT, TRPosition);
            warper.setCorner(ofxGLWarper::BOTTOM_LEFT, BLPosition);
            warper.setCorner(ofxGLWarper::BOTTOM_RIGHT, BRPosition);
            //case where width and hight are negative not clarify

        }
//-----------------------------------------------------------------------------------------------------
        virtual bool insideScaleArea_(int x, int y){ // ORIGIN - WIDTH - HEIGHT - REF

            ofPoint p(x, y);
            p -= ref;
            warper.begin();

            if(     p.x > origin.x + width -resizeArea && p.x < origin.x + width
                    &&  p.y > origin.y + height -resizeArea  && p.y < origin.y + height ){
                        warper.end();
                        return true;
                    }
            warper.end();
            return false;

        }

        //ROTATABLE
//-----------------------------------------------------------------------------------------------------
        void changeAnchor__(float x, float y){ // ANCHOR
            ofPoint p(x, y);
            p -= ref;

            anchor.set(p);
        }
//-----------------------------------------------------------------------------------------------------
        bool insideAnchorArea__(int x, int y){ // ANCHOR - REF

            ofVec2f mouse(x, y); // Absolute
            mouse -= ref;

            if( mouse.distance( anchor ) < 5.0 )
                return true;

            return false;
        }
//-----------------------------------------------------------------------------------------------------
        void rotate_(float r){ // ANGLE - ANCHOR - REF - WARPER

            angle+= r;
            ofVec2f o;

            ofPoint absanchor= anchor + ref; //Absolute

            o= warper.getCorner(ofxGLWarper::TOP_LEFT);

            o.rotate(r, absanchor );
            warper.setCorner(ofxGLWarper::TOP_LEFT, o);

            o= warper.getCorner(ofxGLWarper::TOP_RIGHT);
            o.rotate(r, absanchor );
            warper.setCorner(ofxGLWarper::TOP_RIGHT, o);

            o= warper.getCorner(ofxGLWarper::BOTTOM_LEFT);
            o.rotate(r, absanchor );
            warper.setCorner(ofxGLWarper::BOTTOM_LEFT, o);

            o= warper.getCorner(ofxGLWarper::BOTTOM_RIGHT);
            o.rotate(r, absanchor );
            warper.setCorner(ofxGLWarper::BOTTOM_RIGHT, o);

        }
//-----------------------------------------------------------------------------------------------------
	    float getAngle_(){ // ANGLE
            return angle;
        }
//-----------------------------------------------------------------------------------------------------
	    void setAngle_(float a){ // ORIGIN - ANGLE - ANCHOR - REF - WARPER

            ofVec2f o( origin );
            o.rotate(-angle, anchor );
            o.rotate(a, anchor );
            origin.set( o );

            ofPoint absanchor= anchor + ref;

            o= warper.getCorner(ofxGLWarper::TOP_LEFT);
            o.rotate(-angle, absanchor );
            o.rotate(a, absanchor );
            warper.setCorner(ofxGLWarper::TOP_LEFT, o);

            o= warper.getCorner(ofxGLWarper::TOP_RIGHT);
            o.rotate(-angle, absanchor );
            o.rotate(a, absanchor );
            warper.setCorner(ofxGLWarper::TOP_RIGHT, o);

            o= warper.getCorner(ofxGLWarper::BOTTOM_LEFT);
            o.rotate(-angle, absanchor );
            o.rotate(a, absanchor );
            warper.setCorner(ofxGLWarper::BOTTOM_LEFT, o);

            o= warper.getCorner(ofxGLWarper::BOTTOM_RIGHT);
            o.rotate(-angle, absanchor );
            o.rotate(a, absanchor );
            warper.setCorner(ofxGLWarper::BOTTOM_RIGHT, o);

            angle= a;
        }

        int warpWidth(){ // WARP

            ofPoint B= warper.getCorner(ofxGLWarper::TOP_RIGHT);
            ofPoint C= warper.getCorner(ofxGLWarper::BOTTOM_RIGHT);

            if( B.x  > C.x )
                return B.x - getWarpOrigin().x;

            return C.x - getWarpOrigin().x;
        }

        int warpHeight(){ // WARP

            ofPoint C= warper.getCorner(ofxGLWarper::BOTTOM_RIGHT);
            ofPoint D= warper.getCorner(ofxGLWarper::BOTTOM_LEFT);

            if( C.y > D.y )
                return C.y - getWarpOrigin().y;

            return D.y - getWarpOrigin().y;
        }

        ofPoint getWarpOrigin(){

            ofPoint A= warper.getCorner(ofxGLWarper::TOP_LEFT);
            ofPoint B= warper.getCorner(ofxGLWarper::TOP_RIGHT);
            ofPoint C= warper.getCorner(ofxGLWarper::BOTTOM_RIGHT);
            ofPoint D= warper.getCorner(ofxGLWarper::BOTTOM_LEFT);

            int x= A.x;
            int y= A.y;

            if( B.x < x )
                x= B.x;
            if( B.y < y )
                y= B.y;

            if( C.x < x )
                x= C.x;
            if( C.y < y )
                y= C.y;

            if( D.x < x )
                x= D.x;
            if( D.y < y )
                y= D.y;

            return ofPoint( x, y );
        }

        ofPoint getWarpEnd(){

            ofPoint A= warper.getCorner(ofxGLWarper::TOP_LEFT);
            ofPoint B= warper.getCorner(ofxGLWarper::TOP_RIGHT);
            ofPoint C= warper.getCorner(ofxGLWarper::BOTTOM_RIGHT);
            ofPoint D= warper.getCorner(ofxGLWarper::BOTTOM_LEFT);

            int x= A.x;
            int y= A.y;

            if( B.x > x )
                x= B.x;
            if( B.y > y )
                y= B.y;

            if( C.x > x )
                x= C.x;
            if( C.y > y )
                y= C.y;

            if( D.x > x )
                x= D.x;
            if( D.y > y )
                y= D.y;

            return ofPoint( x, y );
        }



    protected:
        ofxGLWarper warper;
        ofPoint origin;
        ofPoint ref;
        ofPoint anchor; /**< The anchor or the pivot */
        float width;
	    float height;
	    float contWidth;
	    float contHeight;
	    float angle;    /**< Angle in degrees */

        int resizeArea;

	    bool warp;
	    bool anch;
	    bool resizeZone;

};
