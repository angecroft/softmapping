#include "RectNormalMask.h"

/*****************************************/
/* CLASSE CONDAMNEE ( cf RectMask )     */
/****************************************/

RectNormalMask::RectNormalMask(ofPoint R): RectMask(R)
{
    //ctor
    width= 100;
    height= 50;
    cout << "Attention RectNormalMask est obsolete" << endl;
}

RectNormalMask::~RectNormalMask()
{
    //dtor
}

void RectNormalMask::drawPreview(){
    ofPushStyle();
        ofSetColor(255,0,255);
        //img.mirror(true, false);
        img.draw(OM);
    ofPopStyle();
}

void RectNormalMask::drawContent_(){
    ofPushStyle();
        ofSetColor(0,255,255);
        //img.mirror(true, false);
        img.draw(origin.x + 240, origin.y);
    ofPopStyle();
}
