#pragma once

#include "ofMain.h"
#include "ofxFenster.h"
#include "ofxFensterManager.h"

/********************************************//**
 *  \file Out.h
 *  \brief Render
 *
 *  Extra window which contains the mapping-out render.
 ***********************************************/

class Out : public ofxFenster{
public:
	void setup(){
	    //La ligne suivant en commentaire a mettre la ou doit etre cr�er la fenetre.
	    //ofxFensterManager::setup(300,300,OF_WINDOW);
		setWindowShape(200, 200);
		setWindowPosition(310, 310);
		setWindowTitle("Render");

	}

	void update(){
        //content->update();
	}

	void draw(){
	    ofPushStyle();
		ofBackground(0);

        // 1024/700 , 768/500

        //ofRect(ofPoint(), 1024, 768);
        //content->resize(content->width*1.463, content->height*1.536);
        content->resize(srcWidth*1.463, srcHeight*1.536);
        cout << "#_!! " << srcWidth << endl;
        content->draw(vect * ofPoint(1.463, 1.536) + OW );

        ofPopStyle();

	}

	void keyPressed(int key){

	    if( key == 'u' )
            toggleFullscreen();

	}

	void keyReleased(int key){

	}

	void windowResized(int w, int h){
	    setWindowShape(w,h);
    }

    ofVec2f vect;
    ofVec2f OW;
    ofVec2f EW;
	ofImage* content;
	int srcWidth;
	int srcHeight;
};
